import pytest

from webapp.db_models import Timestamp
from flask import current_app


@pytest.mark.usefixtures("testapp")
class TestUrls:
    def test_dashboard_logged_in(self, testapp):
        """ Tests if the dashboard page loads when logged in"""

        rv = testapp.post('/login', data=dict(
            email='admin@utime.de',
            password="admin"
        ), follow_redirects=True)

        rv = testapp.get('/dashboard')
        assert rv.status_code == 200

    def test_dashboard_logged_out(self, testapp):
        """ Tests if the dashboard page (wich is restricted for users) returns a 302 if the user is logged out"""

        rv = testapp.get('/dashboard')
        assert rv.status_code == 302

    def test_add_timestamps_logged_ind(self, testapp):
        """Tests if the timestamp is successfully added when the user logs in"""
        rv = testapp.post('/login', data=dict(
            email='admin@utime.de',
            password="admin"
        ), follow_redirects=True)

        kind = "einstempeln"
        rv = testapp.get('/dashboard/add-timestamps', query_string=dict(
            kind=kind))
        assert rv.status_code == 302

        test_stamps = Timestamp.objects(user_id=0)

        assert test_stamps.as_pymongo().first()["kind"] == "einstempeln"
        test_stamps.delete()
