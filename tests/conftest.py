import sys
from bson.json_util import loads
import json
from pymongo import MongoClient
import os
sys.path.append("../team-e")  # nopep8

import pytest

from webapp.db_models import User
from webapp import create_app
from webapp.config import Config
from webapp import db

client = None


@pytest.fixture()
def testapp(request):

    app = create_app("webapp.config.TestingConfig")
    client = app.test_client()

    # Establish an application context before running the tests
    context = app.app_context()
    context.push()

    yield client

    context.pop()


@pytest.fixture(autouse=True)
def run_around_tests():
    """
    This fixture will run before each test and reset the db to the set test db data
    """

    # Connect to test db
    client = MongoClient(
        # default nur temporär für Abgabe eingefügt
        f"mongodb+srv://{os.environ.get('UTIME_DB_USER', 'uTime_dbUser')}:{os.environ.get('UTIME_DB_PASS', 'uTime_asdf')}"
        f"@utimecluter-mlve5.mongodb.net/{os.environ.get('UTIME_DB_PRODUCTION', 'test_database')}"
        f"?retryWrites=true&w=majority")
    db = client['test_database']

    # Drop existing collections
    db.user.drop()
    db.timestamp.drop()

    # Get the collections
    user_collection = db['user']
    timestammp_collection = db['timestamp']

    # Load test db backups
    with open(os.path.join("tests", "test_db_data", "user_export.json")) as f:
        user_data = f.read()
        user_data = loads(user_data)

    with open(os.path.join("tests", "test_db_data", "timestamp_export.json")) as f:
        timestamp_data = f.read()
        timestamp_data = loads(timestamp_data)

    # Insert test data
    user_collection.insert_many(user_data)
    timestammp_collection.insert_many(timestamp_data)

    # Close connection
    client.close()

    # A test function will be run at this point
    yield
