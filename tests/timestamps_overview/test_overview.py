import pytest

from webapp.db_models import Timestamp
from flask import current_app


@pytest.mark.usefixtures("testapp")
class TestUrls:
    def test_overview_logged_in(self, testapp):
        """ Tests if the overview page loads when logged in"""

        rv = testapp.post('/login', data=dict(
            email='admin@utime.de',
            password="admin"
        ), follow_redirects=True)

        rv = testapp.get('/overview')
        assert rv.status_code == 200

    def test_overview_logged_out(self, testapp):
        """ Tests if the overview page (wich is restricted for users) returns a 302 if the user is logged out"""

        rv = testapp.get('/overview')
        assert rv.status_code == 302
