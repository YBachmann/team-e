import pytest
from datetime import datetime
from mongoengine.queryset import Q
from webapp.db_models import User, Timestamp
from webapp.main.utils import daterange
from webapp.absence_management.utils import get_user_absence_days_in_timeframe
from flask import current_app, url_for


@pytest.mark.usefixtures("testapp")
class TestURLs:
    def test_enter_absence_logged_out(self, testapp):
        """ Tests if the enter_absence page (restricted) returns a 302
            if the user is logged out
        """

        rv = testapp.get('/enter_absence')
        assert rv.status_code == 302

    def test_enter_absence_logged_in(self, testapp):
        """ Tests if the enter_absence route can be used  
            if the user is logged in
        """

        rv = testapp.post('/login', data=dict(
            email='admin@utime.de',
            password="admin"
        ), follow_redirects=True)

        rv = testapp.post('/enter_absence', query_string=dict(
            start_date='01/01/1990',
            end_date='01/02/1990',
            type_of_absence='no_absence',
            pers_nr='0'
        ), follow_redirects=True)

        assert rv.status_code == 200

    def test_enter_absence_no_parameters(self, testapp):
        """ Tests if the user gets redirected to the dashboard
            if '/enter_absence' is called without sufficient parameters. 
        """

        rv = testapp.post('/login', data=dict(
            email='admin@utime.de',
            password="admin"
        ), follow_redirects=True)

        rv = testapp.get('/enter_absence')

        assert rv.location.split("/")[-1] == "dashboard"
        assert rv.status_code == 302

    def test_add_and_remove_absence(self, testapp):
        """ Tests if absence days can be added to and removed from the database successfully.
            Prior to this test there are no existing absence days in the database in this test timeframe. 
        """

        rv = testapp.post('/login', data=dict(
            email='admin@utime.de',
            password="admin"
        ), follow_redirects=True)

        test_start_date_str = "25/05/2020"
        test_end_date_str = "27/05/2020"
        test_start_date_date = datetime.strptime(
            test_start_date_str, "%d/%m/%Y")
        test_end_date_date = datetime.strptime(
            test_end_date_str, "%d/%m/%Y")
        test_user_id = 1

        # All the types that get testet in the following loop
        absence_types_to_test = [
            "urlaub", "krankheit", "zeitausgleich", "home_office"]

        # Run the tests with different absence types
        for test_absence_type in absence_types_to_test:
            ### Add new absences ###
            rv = testapp.post('/enter_absence', query_string=dict(
                start_date=test_start_date_str,
                end_date=test_end_date_str,
                type_of_absence=test_absence_type,
                pers_nr=test_user_id
            ), follow_redirects=True)

            # Query all existing absence days in the specified time frame
            for single_date in daterange(test_start_date_date, test_end_date_date):
                morning = single_date.replace(hour=0, minute=0, second=0)
                evening = single_date.replace(hour=23, minute=59, second=59)
                day = Timestamp.objects(Q(user_id=test_user_id) & Q(
                    date__gte=morning) & Q(date__lte=evening)).as_pymongo().first()

                ### Check if absences got added to database ###
                assert day != None
                assert day["kind"] == test_absence_type

            # Remove the added absences again
            rv = testapp.post('/enter_absence', query_string=dict(
                start_date=test_start_date_str,
                end_date=test_end_date_str,
                type_of_absence="no_absence",
                pers_nr=test_user_id
            ), follow_redirects=True)

            # Query all existing absence days in the specified time frame
            for single_date in daterange(test_start_date_date, test_end_date_date):
                morning = single_date.replace(hour=0, minute=0, second=0)
                evening = single_date.replace(hour=23, minute=59, second=59)
                day = Timestamp.objects(Q(user_id=test_user_id) & Q(
                    date__gte=morning) & Q(date__lte=evening)).as_pymongo().first()

                ### Check if absences got removed from database ###
                assert day == None
