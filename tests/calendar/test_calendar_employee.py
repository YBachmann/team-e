import pytest
from webapp import bcrypt


@pytest.mark.usefixtures("testapp")
class TestURLs:

    # if person tries to access the calendar overview of any employee without being logged in
    def test_calendar_employee_not_logged_in(self, testapp):
        # check if user is redirected to overview_workdays
        rv = testapp.get('/overview_workdays')

        # check if redirect was correct
        assert rv.location.split("/")[-1] == "login?next=%2Foverview_workdays"
        assert 302 == rv.status_code

    # if normal employee tries to access the calendar overview of any employee
    def test_calendar_employee_logged_in_as_employee(self, testapp):
        # data in db of person who is an employee (persNr of Robert = 2)
        rv = testapp.post('/login', data=dict(
            email='robert.leimer@utime.de',
            password="asdf"), follow_redirects=True)

        rv = testapp.get('/overview_workdays?pers_nr=3')
        # check if redirect to dashboard was correct
        assert rv.location.split("/")[-1] == "dashboard"
        assert 302 == rv.status_code

    # if "personalsachbearbeiter" tries to access the calendar overview of himself
    def test_calendar_employee_logged_in_as_personal(self, testapp):
        # admin has rights of personalsachbearbeiter in db
        rv = testapp.post('/login', data=dict(
            email='admin@utime.de',
            password="admin"), follow_redirects=True)

        rv = testapp.get('/overview_workdays?pers_nr=1')
        assert 200 == rv.status_code

    # if person who whats to access is not a personalsachbearbeiter
    # but has the matching persNR
    def test_calendar_employee_logged_in_as_employee_with_access(self, testapp):
        # data in db of person who is an employee with persNR = 2
        rv = testapp.post('/login', data=dict(
            email='robert.leimer@utime.de',
            password="asdf"), follow_redirects=True)
        # according persNR in URL
        rv = testapp.get('/overview_workdays?pers_nr=2')
        assert 200 == rv.status_code

    # if person who wants to access is neither a personalsachbearbeiter
    # nor the person that has the matching persNR
    def test_calendar_employee_logged_in_as_employee_without_access(self, testapp):
        # data in db of person who is an employee with persNR = 2
        rv = testapp.post('/login', data=dict(
            email='robert.leimer@utime.de',
            password="asdf"), follow_redirects=True)

        rv = testapp.get('/overview_workdays?pers_nr=1')
        # check if redirect to dashboard was correct
        assert rv.location.split("/")[-1] == "dashboard"
        assert 302 == rv.status_code

    # if person who wants to access is a personalsachbearbeiter but has not the matching persNR
    def test_calendar_employee_logged_in_as_personal_without_persnr(self, testapp):
        # data in db of person who is an employee with persNR = 1
        rv = testapp.post('/login', data=dict(
            email='admin@utime.de',
            password="admin"), follow_redirects=True)

        rv = testapp.get('/overview_workdays?pers_nr=2')
        assert 200 == rv.status_code

    # test the parameters that are passed in the URL

    # if parameter passed in the URL is empty

    def test_calendar_with_empty_parameters(self, testapp):
        rv = testapp.post('/login', data=dict(
            email='admin@utime.de',
            password="admin"), follow_redirects=True)

        rv = testapp.get('/overview_workdays?pers_nr=')
        # check if redirect to dashboard was correct
        assert rv.location.split("/")[-1] == "dashboard"
        assert 302 == rv.status_code

    # if person who wants to access is a personalsachbearbeiter but has not the matching persNR
    def test_calendar_with_NaN_parameter(self, testapp):
        rv = testapp.post('/login', data=dict(
            email='admin@utime.de',
            password="admin"), follow_redirects=True)

        rv = testapp.get('/overview_workdays?pers_nr=NaN')
        # check if redirect to dashboard was correct
        assert rv.location.split("/")[-1] == "dashboard"
        assert 302 == rv.status_code
