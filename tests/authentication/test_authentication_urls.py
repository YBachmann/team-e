import pytest

from webapp.db_models import User
from flask import current_app


@pytest.mark.usefixtures("testapp")
class TestURLs:
    def test_login(self, testapp):
        """ Tests if the login page loads"""

        rv = testapp.get('/login')
        assert rv.status_code == 200

    def test_logout(self, testapp):
        """ Tests if the logut page loads"""

        rv = testapp.get('/logout')
        assert rv.status_code == 302

    def test_restricted_logged_out(self, testapp):
        """ Tests if the dashboard page (restricted) returns a 302
            if the user is logged out
        """

        rv = testapp.get('/dashboard')
        assert rv.status_code == 302

    def test_restricted_logged_in(self, testapp):
        """ Tests if the dashboard page (restricted) returns a 200
            if the user is logged in
        """

        rv = testapp.post('/login', data=dict(
            email='admin@utime.de',
            password="admin"
        ), follow_redirects=True)

        rv = testapp.get('/dashboard')
        assert rv.status_code == 200

    def test_reset_request(self, testapp):
        """ Tests if the reset_request page loads"""

        rv = testapp.get('/reset_password')
        assert rv.status_code == 200

    def test_reset_token(self, testapp):
        """ Tests if the process of generating and validating a reset token works"""

        # get any user from the db and generate a reset token for this user
        user = User.objects().first()
        token = user.get_reset_token()

        # verify the token again (get the payload (user_id) and query the user with this id)
        user_from_token_payload = user.verify_reset_token(token)

        # check if the User object from the beginning matches the one from the token payload
        assert user == user_from_token_payload
