import pytest


@pytest.mark.usefixtures("testapp")
class TestURLs:
   
    def test_hours_peryear_employee_not_logged_in(self, testapp):
        # overview_hours without log in data

        rv = testapp.get('/hours_peryear_employee')
        # check, if redirecting to hours_peryear_employee page without being logged in
        assert rv.location.split("/")[-1] == "login?next=%2Fhours_peryear_employee"
        assert rv.status_code == 302

    def test_hours_peryear_employee_logged_in_as_employee(self, testapp):
        # call hours_peryear_employee as employee
        testapp.post('/login', data=dict(
            email='robert.leimer@utime.de',
            password="asdf"
        ), follow_redirects=True)

        rv = testapp.get('/hours_peryear_employee')
        assert rv.status_code == 200

    def test_hours_peryear_employee_logged_in_as_personaler(self, testapp):
        # call hours_peryear_employee as personal administrator
        testapp.post('/login', data=dict(
            email='admin@utime.de',
            password="admin"
        ), follow_redirects=True)

        rv = testapp.get('/hours_peryear_employee')
        assert rv.status_code == 200

    

    


