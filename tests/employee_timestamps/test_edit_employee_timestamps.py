import pytest
from datetime import datetime
from mongoengine.queryset import Q
from webapp.db_models import User, Timestamp
from webapp.main.utils import daterange
from webapp.absence_management.utils import get_user_absence_days_in_timeframe
from flask import current_app, url_for


pytest.mark.usefixtures("testapp")


class TestURLs:
    # tests for the edit_timestamps page
    def test_edit_timestamps_logged_out(self, testapp):
        """Tests if the edit-timestamps page returns a 302 if the user is logged out"""

        rv = testapp.get('/employee_timestamps/edit-timestamps')
        assert rv.status_code == 302

    def test_edit_timestamps_logged_in(self, testapp):
        """Tests if the edit-timestamps route can be used if the user is logged in"""

        rv = testapp.post('/login', data=dict(
            email='admin@utime.de',
            password="admin"
        ), follow_redirects=True)

        rv = testapp.post('employee_timestamps/edit-timestamps', query_string=dict(
            date='01.06.2000',
            pers_nr='0'
        ), follow_redirects=True)

        assert rv.status_code == 200

    def test_edit_timestamps_logged_in_without_parameters(self, testapp):
        """Tests if the edit-timestamps route returns to the /employee_timestamps page if there are no parameters"""
        rv = testapp.post('/login', data=dict(
            email='admin@utime.de',
            password="admin"
        ), follow_redirects=True)

        rv = testapp.get('/employee_timestamps/edit-timestamps')

        assert rv.location.split("/")[-1] == "employee_timestamps"
        assert rv.status_code == 302

    # tests for routes, called on the edit_timestamps page
    def test_add_change_delete_timestamps(self, testapp):
        """Tests if Timestamp pairs can be added, changed and deleted from the database successfully.
            Prior to this test there are no existing absence days in the database"""
        rv = testapp.post('/login', data=dict(
            email='admin@utime.de',
            password="admin"
        ), follow_redirects=True)

        test_user_id = 0
        test_stamp_from = "12:15"
        test_stamp_to = "16:03"
        test_date = "08.03.2021"

        # add stamp, start time and end time swapped
        rv = testapp.get('/employee_timestamps/add-timestamps', query_string=dict(
            pers_nr=test_user_id,
            stamp_from=test_stamp_to,
            to=test_stamp_from,
            date=test_date
        ))
        assert rv.status_code == 307

        # sucsessfully add a timestamp-pair
        rv = testapp.get('/employee_timestamps/add-timestamps', query_string=dict(
            pers_nr=test_user_id,
            stamp_from=test_stamp_from,
            to=test_stamp_to,
            date=test_date
        ))
        assert rv.status_code == 200
        # get added timestamp pair and save id for later changing the times
        test_stamps = Timestamp.objects(Q(user_id=test_user_id) & Q(
            kind="einstempeln")).order_by("-pair_id").as_pymongo()
        test_edit_pair_id = (test_stamps[0]["pair_id"])

        # try to add a stamp-pair that overlaps with the previously added pair
        test_stamp_from = "08:00"
        test_stamp_to = "17:00"

        rv = testapp.get('/employee_timestamps/add-timestamps', query_string=dict(
            pers_nr=test_user_id,
            stamp_from=test_stamp_from,
            to=test_stamp_to,
            date=test_date
        ))
        assert rv.status_code == 307

        # edit existing timestamp pair with swapped stamp times
        rv = testapp.get('/employee_timestamps/change-timestamps', query_string=dict(
            pers_nr=test_user_id,
            stamp_from=test_stamp_to,
            to=test_stamp_from,
            date=test_date,
            pair_id=test_edit_pair_id
        ))
        assert rv.status_code == 307

        # sucsessfully edit existing timestamp pair
        rv = testapp.get('/employee_timestamps/change-timestamps', query_string=dict(
            pers_nr=test_user_id,
            stamp_from=test_stamp_from,
            to=test_stamp_to,
            date=test_date,
            pair_id=test_edit_pair_id
        ))
        assert rv.status_code == 200

        # delete the existing timestamp pair
        rv = testapp.get('/employee_timestamps/delete-timestamps', query_string=dict(
            pers_nr=test_user_id,
            pair_id=test_edit_pair_id
        ))
        assert rv.status_code == 200
        # query all timestamps
        test_stamps = Timestamp.objects(
            user_id=test_user_id).as_pymongo().first()

        # check if timestamp pair got deleted
        assert test_stamps == None
