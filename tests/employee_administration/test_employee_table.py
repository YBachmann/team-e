import pytest
from webapp import bcrypt


@pytest.mark.usefixtures("testapp")
class TestURLs:

    # if person tries to access the employee table without being logged in
    def test_employee_table_not_logged_in(self, testapp):
        # check if user is redirected to overview_workdays
        rv = testapp.get('/employee_list')

        # check if redirect was correct
        assert rv.location.split("/")[-1] == "login?next=%2Femployee_list"
        assert 302 == rv.status_code

    # if normal employee tries to access the employee table
    def test_employee_table_logged_in_as_employee(self, testapp):
        # data in db of person who is an employee
        rv = testapp.post('/login', data=dict(
            email='robert.leimer@utime.de',
            password="asdf"), follow_redirects=True)

        rv = testapp.get('/employee_list')
        # check if redirect to dashboard was correct
        assert rv.location.split("/")[-1] == "dashboard"
        assert 302 == rv.status_code

    # if "personalsachbearbeiter" tries to access the employee table
    def test_employee_table_logged_in_as_personal(self, testapp):
        # admin has rights of personalsachbearbeiter in db
        rv = testapp.post('/login', data=dict(
            email='admin@utime.de',
            password="admin"), follow_redirects=True)

        rv = testapp.get('/employee_list')
        assert 200 == rv.status_code

    # check the route that displays all users (also inactive ones)

    # if person tries to access the employee table without being logged in

    def test_employee_table_not_logged_in_active(self, testapp):
        # check if user is redirected to overview_workdays
        rv = testapp.get('/employee_list?state=all')

        # check if redirect was correct
        assert rv.location.split(
            "/")[-1] == "login?next=%2Femployee_list%3Fstate%3Dall"
        assert 302 == rv.status_code

    # if normal employee tries to access the employee table
    # and selects the option to see also deactivated users
    def test_employee_table_logged_in_as_employee_active(self, testapp):
        # data in db of person who is an employee
        rv = testapp.post('/login', data=dict(
            email='robert.leimer@utime.de',
            password="asdf"), follow_redirects=True)

        rv = testapp.get('/employee_list?state=all')
        # check if redirect to dashboard was correct
        assert rv.location.split("/")[-1] == "dashboard"
        assert 302 == rv.status_code

    # if personalsachbearbeiter tries to access the employee table
    # and selects the option to see also deactivated users
    def test_employee_table_logged_in_as_personal_active(self, testapp):
        # admin has rights of personalsachbearbeiter in db
        rv = testapp.post('/login', data=dict(
            email='admin@utime.de',
            password="admin"), follow_redirects=True)

        rv = testapp.get('/employee_list?state=all')
        assert 200 == rv.status_code

    # if "personalsachbearbeiter" tries to access the
    # table with deactivated users in it without parameter in URL
    def test_employee_table_active_users_no_param(self, testapp):
        # admin has rights of personalsachbearbeiter in db
        rv = testapp.post('/login', data=dict(
            email='admin@utime.de',
            password="admin"), follow_redirects=True)

        rv = testapp.get('/employee_list?state=')
        # 200 because that case is already caught in the routes.py
        assert 200 == rv.status_code

    # if "personalsachbearbeiter" tries to access the
    # table with deactivated users in it with wrong parameter in URL
    def test_employee_table_active_users_wrong_param(self, testapp):
        # admin has rights of personalsachbearbeiter in db
        rv = testapp.post('/login', data=dict(
            email='admin@utime.de',
            password="admin"), follow_redirects=True)

        rv = testapp.get('/employee_list?state=SomeWeirdStuff')
        # 200 because that case is already caught in the routes.py
        assert 200 == rv.status_code
