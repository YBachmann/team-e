import pytest
from webapp import bcrypt


@pytest.mark.usefixtures("testapp")
class TestURLs:
    def test_employee_detail_not_logged_in(self, testapp):
        # employee_detail without log in data
        rv = testapp.get('/employee_detail')
        # check, if redirecting to login with initial site as parameters
        assert rv.location.split("/")[-1] == "login?next=%2Femployee_detail"
        assert 302 == rv.status_code

    def test_employee_detail_logged_in_as_employee(self, testapp):
        # call employee_detail as employee
        testapp.post('/login', data=dict(
            email='robert.leimer@utime.de',
            password="asdf"
        ), follow_redirects=True)

        rv = testapp.get('/employee_detail')
        # check, if redirecting to dashboard
        assert rv.location.split("/")[-1] == "dashboard"
        assert 302 == rv.status_code

    def test_employee_detail_logged_in_as_personaler(self, testapp):
        # call employee_detail as personal administrator
        testapp.post('/login', data=dict(
            email='admin@utime.de',
            password="admin"
        ), follow_redirects=True)

        rv = testapp.get('/employee_detail')
        assert 200 == rv.status_code

    def test_employee_detail_new_user_logged_in(self, testapp):
        # employee_detail, create new employee
        hashed_password = bcrypt.generate_password_hash(
            'default').decode("utf-8")
        rv = testapp.post('/employee_detail', data=dict(
            id=1500,
            pers_nr=1500,
            first_name='test',
            last_name='user',
            email='test@user.de',
            image_file='test',
            password=hashed_password,
            street='test',
            house_number=9,
            zip_code=87493,
            city='kempten',
            birthdate='21.12.2019',
            gender=1,
            department='test',
            position='test',
            salary=1500,
            time_model=40,
            role=0,
            holidays=30
        ), follow_redirects=True)

        assert 200 == rv.status_code

    def test_employee_detail_with_persnr_logged_in(self, testapp):
        # call employee_detail with personal number as personal administrator
        testapp.post('/login', data=dict(
            email='admin@utime.de',
            password="admin"
        ), follow_redirects=True)

        rv = testapp.get('/employee_detail?pers_nr=1')
        assert 200 == rv.status_code

    def test_employee_detail_with_non_existent_persnr_logged_in(self, testapp):
        # call employee_detail with non existent personal number as personal administrator
        testapp.post('/login', data=dict(
            email='admin@utime.de',
            password="admin"
        ), follow_redirects=True)

        rv = testapp.get('/employee_detail?pers_nr=2456')
        assert 404 == rv.status_code

    def test_employee_detail_with_wrong_persnr_format_logged_in(self, testapp):
        # call employee_detail with "asdf" as personal number as personal administrator
        testapp.post('/login', data=dict(
            email='admin@utime.de',
            password="admin"
        ), follow_redirects=True)

        rv = testapp.get('/employee_detail?pers_nr=asdf')
        assert rv.location.split("/")[-1] == "dashboard"
        assert 302 == rv.status_code

    def test_employee_change_state_with_persnr_and_reason_logged_in(self, testapp):
        # call employee_change_state with personal number and deactivation reason as personal administrator (deactivate employee)
        testapp.post('/login', data=dict(
            email='admin@utime.de',
            password="admin"
        ), follow_redirects=True)

        rv = testapp.get(
            '/employee_change_state?pers_nr=1&reason=Elternzeit')
        # check, if redirecting to editing employee
        assert rv.location.split("/")[-1] == "employee_detail?pers_nr=1"
        assert 302 == rv.status_code

    def test_employee_change_state_with_persnr_and_no_reason_logged_in(self, testapp):
        # call employee_change_state with personal number but without deactivation reason (activate employee)
        testapp.post('/login', data=dict(
            email='admin@utime.de',
            password="admin"
        ), follow_redirects=True)

        rv = testapp.get('/employee_change_state?pers_nr=1')
        # check, if redirecting to editing employee
        assert rv.location.split("/")[-1] == "employee_detail?pers_nr=1"
        assert 302 == rv.status_code

    def test_employee_change_state_without_persnr_logged_in(self, testapp):
        # call employee_change_state without personal number
        testapp.post('/login', data=dict(
            email='admin@utime.de',
            password="admin"
        ), follow_redirects=True)

        rv = testapp.get('/employee_change_state')
        # check, if redirecting to dashboard
        assert rv.location.split("/")[-1] == "dashboard"
        assert 302 == rv.status_code

    def test_employee_change_state_with_empty_persnr_logged_in(self, testapp):
        # call employee_change_state with an empty personal number
        testapp.post('/login', data=dict(
            email='admin@utime.de',
            password="admin"
        ), follow_redirects=True)

        rv = testapp.get('/employee_change_state?pers_nr=')
        # check, if redirecting to dashboard
        assert rv.location.split("/")[-1] == "dashboard"
        assert 302 == rv.status_code

    def test_employee_change_state_with_wrong_persnr_format_logged_in(self, testapp):
        # call employee_change_state with "asdf" as personal number
        testapp.post('/login', data=dict(
            email='admin@utime.de',
            password="admin"
        ), follow_redirects=True)

        rv = testapp.get('/employee_change_state?pers_nr=assdf')
        # check, if redirecting to dashboard
        assert rv.location.split("/")[-1] == "dashboard"
        assert 302 == rv.status_code

    def test_employee_change_state_with_non_existent_persnr_logged_in(self, testapp):
        # call employee_change_state with "asdf" as personal number
        testapp.post('/login', data=dict(
            email='admin@utime.de',
            password="admin"
        ), follow_redirects=True)

        rv = testapp.get('/employee_change_state?pers_nr=2456')
        assert 404 == rv.status_code
