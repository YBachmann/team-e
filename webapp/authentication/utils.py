from flask import current_app
from flask.helpers import url_for
from flask_mail import Message
from webapp import mail


def send_reset_email(user):
    # Generate a reset token for the specified user
    token = user.get_reset_token()

    # Generate a reset link containing the token
    link = url_for("authentication.reset_token", token=token, _external=True)

    with current_app.app_context():
        # Create the email message
        msg = Message("uTime Password Reset Request",
                      sender="support.utime@gmail.com", recipients=[user.email])
        msg.body = """To reset your password, visit the following link:
        
{}

If you did not make this request then simply ignore this email and no changes will be made. 
""".format(link)

        # Send the mail
        mail.send(msg)
