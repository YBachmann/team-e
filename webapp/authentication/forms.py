from flask_wtf import FlaskForm
from wtforms.fields.core import StringField
from wtforms.validators import DataRequired, Email, EqualTo
from wtforms.fields.simple import PasswordField, SubmitField
from werkzeug.routing import ValidationError
from webapp.db_models import User


class LoginForm(FlaskForm):
    email = StringField("E-Mail",
                        validators=[DataRequired(), Email()])
    password = PasswordField("Password", validators=[DataRequired()])
    submit = SubmitField("Login")


class RequestResetForm(FlaskForm):
    email = StringField("Email", validators=[DataRequired(), Email()])
    submit = SubmitField("Request Password Reset")

    def validate_email(self, email_form):
        # check if a user with this email already exists in the db
        user = User.objects(email=email_form.data).first()
        if user is None:
            raise ValidationError(
                "There is no account with that email. Please register an account. ")


class ResetPasswordForm(FlaskForm):
    password = PasswordField("Password", validators=[DataRequired()])
    # Use the "EqualTo" method to make sure that the user entered the same password in both fields. 
    confirm_password = PasswordField(
        "Confirm Password", validators=[DataRequired(), EqualTo("password")])
    submit = SubmitField("Reset Password")
