from flask import (Blueprint, redirect, request, url_for)
from flask.helpers import flash
from flask.templating import render_template
from flask_login import login_user, logout_user, current_user
from webapp import db, bcrypt
from webapp.db_models import User
from .forms import LoginForm, RequestResetForm, ResetPasswordForm
from .utils import send_reset_email

# Generate the authentication Blueprint
authentication = Blueprint("authentication", __name__,
                           template_folder="templates")


@authentication.route("/", methods=["GET", "POST"])
@authentication.route("/login", methods=["GET", "POST"])
def login():
    """
    This route handles the login process of a user via an email-address and passwort which the user enters. 
    """

    # If the user is already logged in he should not be able to access the login page.
    # Therefore he gets redirected to the dashboard
    if current_user.is_authenticated:
        return redirect(url_for("timestamp_dashboard.dashboard"))

    # Create the LoginForm containing an email-field, a password-field and a submit-button
    loginForm = LoginForm()
    if loginForm.validate_on_submit():
        # Search for a user (an account) that matches the entered email-address
        user = User.objects(email=loginForm.email.data).first()

        # Check if user exists and if the password hashes match
        if user and bcrypt.check_password_hash(user.password, loginForm.password.data):

            # Check if account is inactive
            if user.state == "inaktiv":
                flash("Login Fehlgeschlagen. Ihr Account ist deaktiviert. ", "danger")
                return redirect(request.referrer)

            login_user(user)
            # if user was redirected to login because he was trying to access a restricted route with @login_required
            # grab the next parameter from the url and redirect him where he was trying to go originally
            next_page = request.args.get("next")
            return redirect(next_page) if next_page else redirect(url_for("timestamp_dashboard.dashboard"))
        else:
            flash(
                "Login Fehlgeschlagen. Bitte überprüfen Sie Ihre E-Mail Adresse und Ihr Passwort. ", "danger")

    return render_template("login.html", form=loginForm)


@authentication.route("/logout")
def logout():
    """
    This route logges out the current user
    """
    logout_user()
    return redirect(url_for("authentication.login"))


@authentication.route('/reset_password', methods=["GET", "POST"])
def reset_request():
    """
    This route handles the process of requesting a password reset. 
    """

    # If the user is logged in he should not be able to request a pw-reset. -> Redirect to dashboard
    if current_user.is_authenticated:
        return redirect(url_for("main.dashboard"))

    # Create a form containing an email-field and a submit-button
    # The RequestResetForm makes sure that you have to enter an existing email-address
    form = RequestResetForm()
    if form.validate_on_submit():
        # Grab the user with the entered email from the db
        user = User.objects(email=form.email.data).first()
        send_reset_email(user)
        flash("An email has been sent with instructions to reset your password. ", "info")
        return redirect(url_for("authentication.login"))
    return render_template("reset_request.html", title="Reset Password",
                           form=form)


@authentication.route('/reset_password/<token>', methods=["GET", "POST"])
def reset_token(token):
    """
    This route gets called by klicking on the reset-link. 
    It handles verification of the reset-token and assigning a new password. 

    Args:
        token: reset token containing the user-id as a payload 
    """

    # If the user is logged in he should not be able to change his password. -> redirect to dashboard
    if current_user.is_authenticated:
        return redirect(url_for("main.dashboard"))

    # try to verify the token
    user = User.verify_reset_token(token)

    if user is None:
        flash("This token in invalid or expired. ", "warning")
        return redirect(url_for("authentication.reset_request"))
    else:
        registrationForm = ResetPasswordForm()
        if registrationForm.validate_on_submit():
            # Create a new password hash using the new password
            hashed_password = bcrypt.generate_password_hash(
                registrationForm.password.data).decode("utf-8")

            # Change the password hash in the db
            user.password = hashed_password
            user.save()
            flash(
                "Your password has been updated! Your are now able to log in", "success")
            return redirect(url_for("authentication.login"))
    return render_template("reset_token.html", title="Reset Password",
                           form=registrationForm)
