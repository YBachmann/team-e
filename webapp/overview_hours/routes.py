from flask import Flask
from flask import Blueprint, redirect, request, flash, url_for
from flask_login import login_required, current_user
from flask.templating import render_template
from webapp.db_models import Timestamp
from webapp.db_models import User
from datetime import date, datetime, timedelta
from mongoengine.queryset import Q
import calendar
import json


overview_hours = Blueprint("overview_hours", __name__, template_folder="templates")

@overview_hours.route("/hours_peryear_employee", methods=['GET', 'POST'])
@login_required
def hours_peryear_employee():

    
    persnr = request.args.get("pers_nr")


    # if user is logged in as personaler, show the overview_hours page in employee_list html, else it will be shown in account
    if current_user.role == "1":
        userrole = 1
    

    # get currenst year
    now = datetime.now()
    pastYears = []
    currentYear = now.year

    # get past 10 years and save them in array 
    for i in range(10):
        pastYears.append(currentYear)
        currentYear -= 1

    # get timestamp and user entrys from database
    timestamps = Timestamp.objects(user_id=persnr).all().as_pymongo()
    user = User.objects(pers_nr=persnr).all().as_pymongo()

    

    year = []
    month = []
    hour = []
    Timestampdaten_einstempeln = []
    
    stundenSaldo = 0

    i = 0
    for i in range(len(timestamps)):


        timestampdef = {
            "year": timestamps[i]["date"].year,
            "month": timestamps[i]["date"].month,
            "hour": timestamps[i]["date"].hour,
            # "kind": timestamps[i]["kind"]
           
        }
        # save only valid stamps in array
        if timestamps[i]['kind'] == 'einstempeln' or timestamps[i]['kind'] == 'ausstempeln':
            Timestampdaten_einstempeln.append(timestampdef)
        
        # try to count the ammount of hours from timestamp data and show them later in overview_hours_employee.html
            try:

                stundenSaldo += (timestamps[i+1]["date"] -
                                 timestamps[i]["date"]).total_seconds() / 3600
            except:
                pass
        
        

    
    i += 1

        
    return render_template("overview_hours_employee.html", userrole=userrole, pastYears=pastYears, user=user, stundenSaldo=stundenSaldo )
    
   