from flask import Blueprint, request, redirect, url_for
from flask.helpers import flash
from flask.templating import render_template
from webapp.db_models import Timestamp, User
from flask_login import login_required, current_user
from datetime import datetime, date
from webapp.main.utils import daterange
from .utils import get_user_absence_days_in_timeframe

# Generate the absence_management Blueprint
absence_management = Blueprint(
    "absence_management", __name__, template_folder="templates")


@absence_management.route("/enter_absence", methods=["GET", "POST"])
@login_required
def enter_absence():
    # Check if current user has the rights to enter absences
    if current_user.role != "1":
        flash("Zugriff nur für Personalsachbearbeiter", "danger")
        return redirect(url_for("timestamp_dashboard.dashboard"))

    # Get request parameters from the URL
    start_date = request.args.get("start_date")
    end_date = request.args.get("end_date")
    type_of_absence = request.args.get("type_of_absence")
    pers_nr = request.args.get("pers_nr")

    ########################################
    # Error handling / checking parameters #
    ########################################

    # Check for empty parameters
    if start_date is None or end_date is None or type_of_absence is None or pers_nr is None:
        flash("Ungültige URL. ", "warning")
        return redirect(url_for("timestamp_dashboard.dashboard"))

    # Get datetime objects from request parameters
    try:
        start_date = datetime.strptime(start_date, "%d/%m/%Y")
        end_date = datetime.strptime(end_date, "%d/%m/%Y")
    except ValueError:
        flash("Ungültige Datumsformat. ", "warning")
        return redirect(url_for("timestamp_dashboard.dashboard"))

    if end_date < start_date:
        flash("Das Enddatum (Bis) darf nicht vor dem Startdatum (Von) liegen. ", "warning")
        return redirect(url_for("timestamp_dashboard.dashboard"))

    # Check if type_of_absence is valid
    if type_of_absence not in ["urlaub", "krankheit", "zeitausgleich", "home_office", "no_absence"]:
        flash("Ungültige Absenz Art. ", "warning")
        return redirect(url_for("timestamp_dashboard.dashboard"))

    # Check if user with that pers_nr exists
    user = User.objects(pers_nr=pers_nr).first()
    if user is None:
        flash("Benutzer nicht gefunden. ", "warning")
        return redirect(url_for("timestamp_dashboard.dashboard"))

    #########################
    # Entering the absences #
    #########################

    existing_absence_days = get_user_absence_days_in_timeframe(
        start_date, end_date, user_id_p=pers_nr)

    # no_absence means removing existing absences
    if type_of_absence == "no_absence":
        # check if there are absence days that could be removed
        if not existing_absence_days:
            flash(
                "Es gibt in dem gewählten Zeitraum keine Fehltage, die entfernt werden können. ", "warning")
            return redirect(url_for("calendar.overview_workdays", pers_nr=pers_nr))

        # remove existing absence days
        for absence_day in existing_absence_days:
            day = Timestamp.objects(id=absence_day["_id"]).first()

            # if a vacation day is about to be removed and the vacation is in the future (not taken yet)
            # we give the user back a remeining vacation day
            if day.kind == "urlaub" and day.date.date() > date.today():
                user.remaining_holidays += 1
                user.save()
            day.delete()

        flash("Fehltage erfolgreich entfernt", "success")
        return redirect(url_for("calendar.overview_workdays", pers_nr=pers_nr))
    else:
        # If there are already absence days in the specified timeframe those have to be removed first using "no_absence"
        if existing_absence_days:
            flash("Bitte erst bestehende Fehltage entfernen", "danger")
            return redirect(url_for("calendar.overview_workdays", pers_nr=pers_nr))
        else:
            if type_of_absence == "urlaub":
                vacation_duration = int((end_date - start_date).days) + 1
                if vacation_duration > User.objects.get(pers_nr=pers_nr).holidays:
                    flash(
                        "Der Mitarbeiter hat nicht genügend Ürlaubstage für den gewählten Zeitraum.", "warning")
                    return redirect(url_for("calendar.overview_workdays", pers_nr=pers_nr))
                else:
                    user.remaining_holidays -= vacation_duration
                    user.save()

            # save new absence days to db
            for absence_day in daterange(start_date, end_date):
                new_absence_day = Timestamp(
                    user_id=pers_nr, date=absence_day, kind=str(type_of_absence))
                new_absence_day.save()

        flash("Fehltage erfolgreich hinzugefügt", "success")
        return redirect(url_for("calendar.overview_workdays", pers_nr=pers_nr))
