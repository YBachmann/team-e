from datetime import datetime, timedelta
from webapp.main.utils import daterange
from webapp.db_models import Timestamp
from mongoengine.queryset import Q


def get_user_absence_days_in_timeframe(start_date, end_date, user_id_p=1):
    """
    Create and return a list of all absence days of a user in a specified timeframe. 

    Args:
        start_date (datetime.datetime): Dates between and including start_date and end_date get added to the query
        end_date (datetime.datetime): Dates between and including start_date and end_date get added to the query
        user_id_p=1 (int): The user_id of the user whose absences should be queried

    Return: 
        existing_absence_days[] (db_models.Timestamp): List of all absence days of a user between and including start_date and end_date
    """
    existing_absence_days = []
    for single_date in daterange(start_date, end_date):
        morning = single_date.replace(hour=0, minute=0, second=0)
        evening = single_date.replace(hour=23, minute=59, second=59)

        # Query db for timestamp in the current single_date. 
        # We are only interested in timestamps that are absences, 
        # therefore we can use .first() as a day with an absence can only contain one timestamp. 
        day = Timestamp.objects(Q(user_id=user_id_p) & Q(
            date__gte=morning) & Q(date__lte=evening)).as_pymongo().first()

        # Only add actual absence days to the list. Ignore stamp ins/outs. 
        if day is not None and day['kind'] != 'einstempeln' and day['kind'] != 'ausstempeln':
            existing_absence_days.append(day)

    return existing_absence_days
