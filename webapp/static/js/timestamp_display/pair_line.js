/**Class representing a line, connecting two timestamps. */
class PairLine {
    /**
     * Set up a line object
     * @param {number} posFrom Start of line
     * @param {number} posTo End of line
     */
    constructor(posFrom, posTo) {
        // settings
        this.posFrom = posFrom;
        this.posTo = posTo;
        // dom elements
        this.line = document.createElement("div");
    }
    /**
     * create a line
     */
    create() {
        this.makeLine();
    }
    /**
     * update the position of the line end
     * @param {*} posTo 
     */
    update(posTo) {
        this.line.style.width = `${posTo - this.posFrom}px`;
    }
    /**
     * display the line
     * @param {*} timeline 
     */
    append(timeline) {
        timeline.appendChild(this.line);
    }
    /**
     * set up line dom
     */
    makeLine() {
        // set "id" attribute to work with css formatting
        this.line.setAttribute("id", "line");
        this.line.style.marginLeft = `${this.posFrom}px`;
        this.line.style.width = `${this.posTo - this.posFrom}px`;

    }
}