/** Class managing the display of one day.
 * Added timestamp pairs are connected with a line
*/
class Day {
    /**
     * Set up the display of one day.
     * @param {Date} date 
     * @param {settings} s 
     * @param {String} color 
     */
    constructor(date, s, color) {
        // settings
        this.displayArea = s.displayArea;
        this.date = date;
        this.color = color;
        this.edit = s.edit;
        //array holds to hold timestamps and lines
        this.pairs = [];
        //dom elements
        this.tag = document.createElement("div");
        this.timeline = document.createElement("div");
        this.text = document.createElement("div")
        this.button = document.createElement("i")
    }

    /**
     * Create Display for one day
     */
    create() {
        // set "id" attribute to work with css formatting
        this.timeline.setAttribute("id", "timeline");
        // set styling
        this.timeline.style.background = this.color;
        // create the date-tag
        this.makeTag();
        // if the "edit-timestamps" butten should be displayd, create it.
        if (s.edit) {
            this.timeline.setAttribute("class", "edit_timeline")

            this.makeButton();
        }
    }

    /**Show the day in the display area */
    append() {
        this.pairs.forEach(element => {
            element.forEach(entry => {
                entry.append(this.timeline);
            })
        });
        // if the text element is used (to display an absence day) append it
        if (this.text.innerHTML != "") {
            this.timeline.appendChild(this.text);
        }
        // if the "edit-timestamps" butten should be displayed
        if (this.edit) {
            this.timeline.appendChild(this.button);
        }
        this.timeline.appendChild(this.tag)

        this.displayArea.appendChild(this.timeline);

        //set the height of the display area according to the content
        this.displayArea.parentElement.style.height = `${this.timeline.getBoundingClientRect().bottom}px`
    }

    /**
     * Add a timestamp pair to the day and connect it with a line
     * @param stampIn Timestamp object 
     * @param {timestamp} stampOut 
    */
    addStampPair(stampIn, stampOut) {
        // create a line starting at the position of stampIn and ending at the position of stampOut
        var line = new PairLine(stampIn.position, stampOut.position)
        line.create();
        // add the timestamp pair and the line to the array, containing all stamps for this day
        this.pairs.push([line, stampIn, stampOut]);
    }

    /**Add a single timestamp to the day */
    addSingleStamp(stamp) {
        this.pairs.push([stamp]);
    }

    /**Add a label to the display (red when "krankheit", yellow when "urlaub") */
    addText(kindOfText) {
        // set "id" attribute represent the kind of absence day
        if (kindOfText == "urlaub" || kindOfText == "krankheit") {
            this.text.setAttribute("id", kindOfText);
        }
        else {
            this.text.setAttribute("id", "absense")
        }
        this.text.innerHTML = kindOfText;
    }

    /**Show the date as "d.m.yyyy" in Front of the day display */
    makeTag() {
        // set "id" attribute to work with css formatting
        this.tag.setAttribute("id", "dateTag");
        this.dateString = `${this.date.getDate()}.${this.date.getMonth() + 1}.${this.date.getFullYear()}`;
        this.tag.innerHTML = this.dateString
    }

    /**Add a button to the day redirecting to the edit-timestamp page */
    makeButton() {
        // set "id" attribute to work with css formatting
        this.button.setAttribute("id", "edit")
        this.button.setAttribute("class", "fas fa-edit fa-lg")
        //add the date attribute to the button element to be later used by the onclick function
        this.button.setAttribute("date", this.dateString)

        // set styleing to the button
        this.button.style.position = "absolute";
        this.button.style.marginTop = "-15px";
        this.button.style.left = "82px";

        //add an onclick function to the button
        this.button.onclick = function () {
            // get the date, saved in the "date" attribute
            var date = this.getAttribute('date');
            // get the current url
            var url = new URL(window.location.href)
            //get the persnr of the employee
            var pers_nr = url.searchParams.get("pers_nr");
            //call the edit-timestamps page with the date and persnr as parameters
            window.location.href = `/employee_timestamps/edit-timestamps?date=${date}&pers_nr=${pers_nr}`
        }
    }
}
