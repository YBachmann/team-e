
const minutesPerHour = 60;

var s;
var TimestampCollection = {

    //overall used variables, objects, etc..
    settings: {
        //settings to choose..
        date: new Date(),
        //How many hours will be displayed
        numHours: 8,
        //Element in wich the display will be added
        displayArea: $("#timestampView")[0],
        timeline: document.createElement("div"),
        startTime: new Date(),
        //how many hours will be displayed by the ruler (1 = every hour, 2 = every second ...)
        step: 2,
        //calculated variables
        scale: 0.5,
        //should there be an "edit-timestamps" button?
        edit: false,
    },

    /**
     * Initializes the timestampdisplay
     * @param {Boolean} edit 
     */
    init: function (edit) {
        s = this.settings;

        s.edit = edit;
        // set the beginning of the scale to the first timestamp
        if (dayStamps.length > 0) {
            s.date = new Date(dayStamps[0]["date"]["$date"])
            // get rid of timezone difference
            s.date.setHours(s.date.getHours() - 2)
        }
        // set the beginning of the scale to the current time
        else {
            s.date = new Date()
        }
        // set the scale
        this.setScale();
        // set the start time
        this.setStartTime();
        // create a ruler with settings from above
        ruler = new Ruler(s.startTime, s.numHours, s.step, s.scale);
        // add ruler to the display area
        s.displayArea.appendChild(ruler)

        s.timeline.setAttribute("id", "timeline");
        // convert the start date to a Date object
        // start_date will be used to the date-tag
        start_date = new Date(Date.parse(statdate))
        this_date = new Date(start_date)
        var stampin, stampout

        // create a new display for one day
        day = new Day(this_date, s)
        day.create();
        if (dayStamps.length > 0) {
            dayStamps.forEach(function (stamp, i) {

                //if there is only one "einstempeln" stamp but no "ausstempeln" stamp display the stamp together with a livestamp, connected by a line
                if (stamp["kind"] == "einstempeln" && !dayStamps[i + 1]) {
                    //convert Datetime Object to JS-Date Object
                    stamp_time = new Date(stamp["date"]["$date"])
                    stamp_time.setHours(stamp_time.getHours() - 2)
                    //create a "einstempeln" stamp
                    stampin = new Timestamp("start", stamp_time, s);
                    stampin.create();

                    //If the displayed livestamp would be to close to the "einstempeln" stamp it wont be created
                    var diffMins = Math.round(((new Date - stamp_time % 86400000) % 3600000) / 60000); // minutes
                    if (diffMins < 30) {
                        day.addSingleStamp(stampin);
                    }
                    else {
                        //create a live stamp
                        stamp_time = new Date();
                        livestamp = new Timestamp("live", stamp_time, s, 0);
                        livestamp.create();

                        day.addStampPair(stampin, livestamp);
                    }

                }
                // if there is a "einstempeln" stamp and also a "ausstempeln" stamp display them both with a connecting line
                else if (stamp["kind"] == "einstempeln" && dayStamps[i + 1]["kind"] == "ausstempeln") {
                    //convert Datetime Object to JS-Date Object
                    stamp_time = new Date(stamp["date"]["$date"])
                    stamp_time.setHours(stamp_time.getHours() - 2)
                    //create a "einstempeln" stamp
                    stampin = new Timestamp("start", stamp_time, s);
                    stampin.create();

                    //convert Datetime Object to JS-Date Object
                    stamp_time = new Date(dayStamps[i + 1]["date"]["$date"])
                    stamp_time.setHours(stamp_time.getHours() - 2)
                    //create a "ausstempeln" stamp
                    stampout = new Timestamp("end", stamp_time, s);
                    stampout.create();

                    day.addStampPair(stampin, stampout);
                }
                // if the is neither a "einstempeln" stamp, nor a "ausstempeln" stamp (for example holliday or sickday) just display the text
                else if (stamp["kind"] != "einstempeln" && stamp["kind"] != "ausstempeln") {
                    day.addText(stamp["kind"]);
                }
            })
        }
        // if there are no timestamps display a live stamp
        else {
            stamp_time = new Date();
            livestamp = new Timestamp("live", stamp_time, s, 0);
            livestamp.create();
            day.addSingleStamp(livestamp)
        }


        day.append();
    },

    //Sets the Scale according to the predefined Area in HTML
    setScale: function () {
        s.scale = s.displayArea.getBoundingClientRect().width / (s.numHours * minutesPerHour);
    },
    // set the starttime according to the first timestamp.
    setStartTime: function () {
        // the start time is at least 30 minutes before the first timestamp and at max one hour before it.
        s.startTime.setHours(s.date.getHours() - 1 + Math.round(s.date.getMinutes() / 60));
        s.startTime.setMinutes(0, 0, 0);
    }

}