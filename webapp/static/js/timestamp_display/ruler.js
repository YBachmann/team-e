/**Class representing the ruler, showing the timespan */
class Ruler {
    /**
     * Set up the display of the ruler
     * @param {Date} time 
     * @param {number} numHours 
     * @param {number} step 
     * @param {number} scale 
     */
    constructor(time, numHours, step, scale) {
        //settings
        this.time = new Date(time)
        this.range = numHours / step;
        this.step = step;
        this.scale = scale;
        this.numHours = numHours;
        // elements
        this.ruler = document.createElement('div');
        this.line = document.createElement('div');
        this.hours = document.createElement('div');

        this.create();
        return this.ruler;
    }

    /**
     * create the display of the ruler
     */
    create() {
        this.ruler.setAttribute("id", "scale_group");
        this.populateHours();
        this.makeLine();

    }

    /**
     * delete the display of the ruler
     */
    delete() {
        this.ruler.removeChild(this.line);
        this.ruler.removeChild(this.hours)
        return this.ruler;
    }

    /**
     * set the start time for the Ruler
     * @param {Date} startTime 
     */
    setStartTime(startTime) {
        this.time = new Date(startTime);
    }

    /**
     * Add the hours to the ruler
     */
    populateHours() {
        var tempTime = new Date(this.time)
        var i = 0;
        var tempHours = document.createElement('div');
        for (i; i <= this.range; i++) {
            var hour = document.createElement('div');
            hour.setAttribute("class", "hour");
            // set position of the displayed text according to the time it displays
            hour.style.marginLeft = (i * 60 * this.step * this.scale) + "px";
            //add the text to the hour object
            hour.innerHTML = this.hourAsString(tempTime);
            //add the "step" (defined in the settings) to the hour
            tempTime.setHours(tempTime.getHours() + this.step);
            tempHours.appendChild(hour);
        }
        this.hours = tempHours;
        this.ruler.appendChild(this.hours);
    }

    /**
     * show a line beneth the hours
     */
    makeLine() {
        this.line.setAttribute("id", "scale_line");
        this.line.style.width = `${this.scale * this.numHours * 60}px`;
        this.ruler.appendChild(this.line);
    }

    /**
     * convert a Date object to a string
     * adding leading 0 if hour is < 10 and 00 for the minutes
     * @param {Date} tempTime 
     */
    hourAsString(tempTime) {
        var hour = tempTime.getHours();
        if (hour < 10) {
            return '0' + hour + ":00";
        }
        else {
            return hour + ":00";
        }
    }
}