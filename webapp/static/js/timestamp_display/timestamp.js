/**
 * Class managing the display of one timestamp.
 */
class Timestamp {
    /**
     * Set up creation of one timestamp
     * @param {String} kind "start", "end" or "live"
     * @param {Date} time  
     * @param {*} s Settings
     */
    constructor(kind, time, s) {
        //settings
        this.kind = kind;
        this.time = time
        this.timeline = s.timeline;
        this.startTime = new Date(s.startTime);
        this.scale = s.scale;
        //dom elements
        this.group = document.createElement('div');
        this.bullet = document.createElement('div');
        this.tag = document.createElement('span');
        // initialize position
        this.setPosition();
    }
    /**
     * create a day
     */
    create() {
        this.makeBullet();
        this.makeTag();
        this.makeGroup();
    }

    /**
     * update the position of the timestamp
     * @param {Date} time 
     */
    update(time) {
        this.time = new Date(time)
        this.setPosition();
        this.group.style.marginLeft = `${this.position}px`;
        this.tag.innerHTML = this.timeAsString();
    }

    /**
     * Delete the timestamp
     */
    delete() {
        this.group.removeChild(this.bullet);
        this.group.removeChild(this.tag);
        this.timeline.removeChild(this.group);
    }

    /**
     * Display the timestamp
     * @param {*} timeline Day to append timestamp to
     */
    append(timeline) {
        this.timeline = timeline;

        timeline.appendChild(this.group);
        this.group.appendChild(this.bullet)
        this.group.appendChild(this.tag);
    }

    /**
     * create a group to hold the bullet and the tag
     */
    makeGroup() {
        this.group.setAttribute("class", "stamp")
        this.group.style.marginLeft = `${this.position}px`;
    }

    /**
     * create the bullet
     */
    makeBullet() {
        this.bullet.setAttribute("id", `${this.kind}_point`);
    }

    /**
     * create the tag
     */
    makeTag() {
        this.tag.setAttribute("id", `${this.kind}_time`);
        this.tag.innerHTML = this.timeAsString(this.time);
    }

    /**
     * set the position of the timestamp according to the scale and time
     */
    setPosition() {
        //calculate minutes since the starttime of the display
        var tempMin = this.time.getMinutes() + ((this.time.getHours() - this.startTime.getHours()) * 60);
        // if the stamp belongs to the next day add 24 hours to the minutes since start
        // otherwise it would be negative
        if (tempMin < 0) {
            console.log("stamp from next day");
            tempMin = this.time.getMinutes() + (((this.time.getHours() + 24) - this.startTime.getHours()) * 60);
        }
        // multiply the minutes by the scaling (defined in the settings)
        this.position = tempMin * this.scale;
    }

    /**
     * Convert the time to a string ("HH:MM")
     */
    timeAsString(time) {
        var tempMin = time.getMinutes();
        // adding leading 0 to the minute if it is < 10
        if (tempMin < 10) tempMin = `0${tempMin}`

        var tempHour = time.getHours();
        // adding leading 0 to the hour if it is < 10
        if (tempHour < 10) tempHour = `0${tempHour}`

        // returning string combining hour and minute
        return `${tempHour}:${tempMin}`
    }
}