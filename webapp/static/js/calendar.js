//todays date
let today = new Date();
//current year/month
let currentMonth = today.getMonth();
let currentYear = today.getFullYear();
//year selected in the picker below calendar
let selectYear = document.getElementById("year");

let months = ["Januar", "Februar", "März", "April", "Mai", "Juni",
    "Juli", "August", "September", "Oktober", "November", "Dezember"];

//list of cards/calendars
DivList = document.getElementsByClassName("card");

let k = 0;

//iterate through every calendar
for (Div of DivList) {
    //add months as headers
    var h3 = document.createElement("h3");
    h3.classList.add("card-header");
    h3.classList.add("monthAndYear");
    h3.innerHTML = months[k] + " " + currentYear
    Div.insertBefore(h3, Div.firstChild);
    k++;
} //create header of calendar


let monthAndYear = document.getElementById("monthAndYear");
showCalendar(currentYear);

/**
 * function that changes the year displayed by the calendars if yearpicker changes value
 */
function jump() {
    currentYear = parseInt(selectYear.value);
    showCalendar(currentYear);
}

/** function displays the 12 calendars on the overview over the workdays
 * calculates the right order and number of days within the calendars
 * colors the cells of the calendars depending on the data in the timestamps
 * 
 * @param {number of the current year to calculate the right order} year 
 */
function showCalendar(year) {

    //list of cards/calendars
    h3List = document.getElementsByClassName("card-header");
    let k = 0;
    for (h3 of h3List) {
        h3.innerHTML = "";
        h3.innerHTML = months[k] + " " + year
        k++;
    }
    // body of the calendar
    let tbllist = document.getElementsByClassName("calendar-body");
    //index that depicts months (january = 0, december = 11)
    let z = 0;

    for (let tbl of tbllist) {

        //get weekday of first day in month
        let firstDay = parseInt((new Date(year, z)).getDay());
        //get number of days in a month
        let daysInMonth = 32 - new Date(year, z, 32).getDate();

        // clearing all previous cells
        tbl.innerHTML = "";

        // filing data about month and in the page via DOM.
        selectYear.value = year;

        // creating all cells
        let date = 1;
        for (let i = 0; i < 6; i++) {
            // creates a table row
            let row = document.createElement("tr");

            //creating individual cells, filing them up with data.
            for (let j = 0; j < 7; j++) {

                //fill empty cells at beginning of month with placeholders
                if (i === 0 && j < firstDay) {
                    let cell = document.createElement("td");
                    cell.classList.add("border-black");
                    let cellText = document.createTextNode("");
                    cell.appendChild(cellText);
                    row.appendChild(cell);
                }

                //break clause to set only as many days as there are in the month
                else if (date > daysInMonth) {
                    break;
                }

                //fill days in month with proper data
                else {
                    let cell = document.createElement("td");
                    let cellText = document.createTextNode(date);
                    // color today's date
                    if (date === today.getDate() && year === today.getFullYear() && z === today.getMonth()) {
                        cell.classList.add("border-thick");
                    }
                    else {
                        cell.classList.add("border-black");
                    }
                    // color workdays
                    //go through all timestamps and check if cells have to be painted
                    for (let h = 0; h < dayTimestamp.length; h++) {

                        if (date === dayTimestamp[h] && z === monthTimestamp[h] - 1 && year === yearTimestamp[h]
                            && kindTimestamp[h] === "einstempeln") {
                            cell.classList.add("bg-workday");
                        }
                        else if (date === dayTimestamp[h] && z === monthTimestamp[h] - 1 && year === yearTimestamp[h]
                            && kindTimestamp[h] === "krankheit") {
                            cell.classList.add("bg-sickday");
                        }
                        else if (date === dayTimestamp[h] && z === monthTimestamp[h] - 1 && year === yearTimestamp[h]
                            && kindTimestamp[h] === "urlaub") {
                            cell.classList.add("bg-holiday");
                        }
                        else if (date === dayTimestamp[h] && z === monthTimestamp[h] - 1 && year === yearTimestamp[h]
                            && kindTimestamp[h] === "home_office") {
                            cell.classList.add("bg-home_office");
                        }
                        else if (date === dayTimestamp[h] && z === monthTimestamp[h] - 1 && year === yearTimestamp[h]
                            && kindTimestamp[h] === "zeitausgleich") {
                            cell.classList.add("bg-zeitausgleich");
                        }
                    }

                    cell.appendChild(cellText);
                    row.appendChild(cell);
                    date++;
                }
            }
            // appending each row into calendar body.
            tbl.appendChild(row);
        }
        //iterate month
        z++;
    }

}
