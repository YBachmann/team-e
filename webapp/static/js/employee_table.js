/** 
* Generate the employee table with mdb-4 datatables2 
*/
$(document).ready(function () {

    var table = $('#employeeTable').DataTable({
        // positioning of the table items
        // l = "show X entries" item
        // f = search input
        // r = processing display element
        // t = table
        // B = buttons
        // p = pagination
        // l and f at the top of the table
        // r and t in the middle
        // B and P at the bottom of the table
        dom: '<"top"lf>rt<"bottom"Bp><"clear">',
        //some extra functions
        // the buttons to show and add to the table
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        // specifiy the columns of the table
        columns: [
            { title: "PersNr" },
            { title: "Vorname" },
            { title: "Name" },
            { title: "Position" },
            {
                title: 'Anwesenheitsstatus',
                "className": 'status',
                "orderable": true,
                //add status depending on value in row
                "render": function (row) {
                    if (row == 0) {
                        return '<i class="fas fa-check fa-lg"></i>';
                    } else if (row == 1) {
                        return 'Urlaub';
                    } else if (row == 2) {
                        return 'Krank';
                    } else if (row == 3) {
                        return 'Fortbildung';
                    } else if (row == 4) {
                        return 'Home Office';
                    } else if (row == 5) {
                        return 'Abwesend';
                    } else {
                        return 'Error';
                    }
                }
            },
            {
                //added icons
                "className": 'stamp',
                "orderable": false,
                "data": null,
                "visible": true,
                "defaultContent": '<i id="stamp" class="fas fa-stamp fa-lg"></i>'
            },

            {
                "className": 'calendar',
                "orderable": false,
                "data": null,
                "visible": true,
                "defaultContent": '<i id="calendar" class="fas fa-calendar-alt fa-lg"></i>'
            },

            {
                "className": 'edit',
                "orderable": false,
                "data": null,
                "visible": true,
                "defaultContent": '<i id="edit" class="fas fa-edit fa-lg"></i>'
            },

            {
                "className": 'hours_overview',
                "orderable": false,
                "data": null,
                "visible": true,
                "defaultContent": '<i id="hours_overview" class="fas fa-chart-pie fa-lg"></i>'
            }

        ],
        // order the table on column 0 "pers_nr" ascending
        "order": [[0, 'asc']
        ],
        // pagination is activated and with "simple_numbers" only arrows to left and right and the page numbers are shown
        "paging": true,
        "pagingType": "simple_numbers",
    });
    $('.dataTables_length').addClass('bs-select');

    $('#employeeTable tbody').on('click', '#hours_overview', function (e) {
        // when the hours_overview-icon is clicked, call the hours_peryear_employee route with the corresponding personal number
        var data = table.row($(this).parents('tr')).data();
        window.location.href = '/hours_peryear_employee?pers_nr=' + data[0];
    });
    //click events on buttons
    $('#employeeTable tbody').on('click', '#stamp', function (e) {
        var data = table.row($(this).parents('tr')).data()
        window.location.href = 'employee_timestamps?pers_nr=' + data[0]
    });
    $('#employeeTable tbody').on('click', '#calendar', function (e) {
        var data = table.row($(this).parents('tr')).data();
        window.location.href = '/overview_workdays?pers_nr=' + data[0];
    });
    $('#employeeTable tbody').on('click', '#edit', function (e) {
        // when the edit-icon is clicked, call the employee_detail route with the corresponding personal number
        var data = table.row($(this).parents('tr')).data();
        window.location.href = '/employee_detail?pers_nr=' + data[0];
    });
    
});
