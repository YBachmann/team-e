//same procedure like in calendar.js but this time with only one calendar
let today = new Date();
let currentMonth = today.getMonth();
let currentYear = today.getFullYear();

let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

let monthAndYear = document.getElementById("monthAndYear");
showCalendar(currentMonth, currentYear);

/** function displays the single calendar on the dashboard
 * calculates the right order and number of days within the calendar
 * colors the cells of the calendar depending on the data in the timestamps
 * 
 * @param {index of the current month starting by 0 (->january = 0, february = 1, ...) to calculate the right order} month 
 * @param {number of the current year also to calculate the right order} year 
 */
function showCalendar(month, year) {

    let firstDay = (new Date(year, month)).getDay();
    let daysInMonth = 32 - new Date(year, month, 32).getDate();

    let tbl = document.getElementById("calendar-body"); // body of the calendar

    // clearing all previous cells
    tbl.innerHTML = "";

    // filing data about month and in the page via DOM.
    monthAndYear.innerHTML = months[month] + " " + year;

    // creating all cells
    let date = 1;
    for (let i = 0; i < 6; i++) {
        // creates a table row
        let row = document.createElement("tr");

        //creating individual cells, filing them up with data.
        for (let j = 0; j < 7; j++) {
            if (i === 0 && j < firstDay) {
                let cell = document.createElement("td");
                cell.classList.add("border-black");
                let cellText = document.createTextNode("");
                cell.appendChild(cellText);
                row.appendChild(cell);
            }
            else if (date > daysInMonth) {
                break;
            }

            else {
                let cell = document.createElement("td");
                let cellText = document.createTextNode(date);
                if (date === today.getDate() && year === today.getFullYear() && month === today.getMonth()) {
                    cell.classList.add("border-thick");
                } // color today's date
                else {
                    cell.classList.add("border-black");
                }

                //go through all timestamps and check if cells have to be painted
                for (let h = 0; h < dayTimestamp.length; h++) {

                    if (date === dayTimestamp[h] && month === monthTimestamp[h] - 1 && year === yearTimestamp[h]
                        && kindTimestamp[h] === "einstempeln") {
                        cell.classList.add("bg-workday");
                    }

                    else if (date === dayTimestamp[h] && month === monthTimestamp[h] - 1 && year === yearTimestamp[h]
                        && kindTimestamp[h] === "krankheit") {
                        cell.classList.add("bg-sickday");
                    }
                    else if (date === dayTimestamp[h] && month === monthTimestamp[h] - 1 && year === yearTimestamp[h]
                        && kindTimestamp[h] === "urlaub") {
                        cell.classList.add("bg-holiday");
                    }
                    else if (date === dayTimestamp[h] && month === monthTimestamp[h] - 1 && year === yearTimestamp[h]
                        && kindTimestamp[h] === "home_office") {
                        cell.classList.add("bg-home_office");
                    }
                    else if (date === dayTimestamp[h] && month === monthTimestamp[h] - 1 && year === yearTimestamp[h]
                        && kindTimestamp[h] === "zeitausgleich") {
                        cell.classList.add("bg-zeitausgleich");
                    }
                }

                cell.appendChild(cellText);
                row.appendChild(cell);
                date++;
            }
        }
        // appending each row into calendar body
        tbl.appendChild(row);
    }

}