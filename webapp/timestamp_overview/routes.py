from flask import (Blueprint, redirect, request, url_for)
from flask.helpers import flash, session
from flask.templating import render_template
from flask_login import login_user, logout_user, current_user, login_required
from flask import redirect,  request, jsonify, make_response, json
from webapp.db_models import Timestamp
from datetime import datetime, timedelta
from mongoengine import Q
from webapp.main.utils import *


timestamp_overview = Blueprint(
    'timestamp_overview', __name__, template_folder="templates")


@timestamp_overview.route("/")
@timestamp_overview.route("/overview")
@login_required
def overview():
    # get Timestamps off current month til today
    today = datetime.now()
    month = today.month
    daydate = today.day
    start = datetime(today.year, month, 1)
    end = datetime(today.year, month, daydate)
    all_days = []

    # Iterate over every day from start date to end date.
    # single_date is a datetime object
    for single_date in daterange(start, end):
        # query all timestamps from one day by searching for every entry between morning and evening
        morning = single_date.replace(hour=0, minute=0, second=0)
        evening = single_date.replace(hour=23, minute=59, second=59)
        # query for timestamps where the date is 'greater than or equal'(gte) morning date and 'less than or equal' (lte) evening date
        day = Timestamp.objects(Q(user_id=current_user.pers_nr) & Q(
            date__gte=morning) & Q(date__lte=evening)).order_by('date').as_pymongo()
        # add the day to an array containing all days of the month
        all_days.append(day)

    return render_template('overview.html', timestamps=json.dumps(all_days), startdate=start, end_date=end)


@timestamp_overview.route("/overview/get-timestamps", methods=["POST", "GET"])
@login_required
def get_timestamps():
    req = request.get_json()
    all_days = []
    # convert date strings to datetime objects
    # and check for faulty dates
    try:
        start_date = datetime.strptime(req["from"], '%d.%m.%Y')
        end_date = datetime.strptime(req["to"], '%d.%m.%Y')
    except ValueError:
        flash("Kein gültiges Datum erkannt", "danger")

        res = make_response("No Dates", 404)
        return res

    # check if enddate is before start date
    if end_date < start_date:
        flash("Das Enddatum (Bis) darf nicht vor dem Startdatum (Von) liegen. ", "danger")
        res = make_response("fals Dates", 404)
        return res

    # Iterate over every day from start date to end date.
    # single_date is a datetime object
    for single_date in daterange(start_date, end_date):
        # query all timestamps from one day by searching for every entry between morning and evening
        morning = single_date.replace(hour=0, minute=0, second=0)
        evening = single_date.replace(hour=23, minute=59, second=59)
        # query for timestamps where the date is 'greater than or equal'(gte) morning date and 'less than or equal' (lte) evening date
        day = Timestamp.objects(Q(user_id=current_user.pers_nr) & Q(
            date__gte=morning) & Q(date__lte=evening)).order_by('date').as_pymongo()
        # add the day to an array containing all days of the timespan
        all_days.append(day)
    # create a response containing all the timestamps and the startdate
    res = make_response(jsonify({"timestamps": json.dumps(
        all_days), "startdate": start_date, "end_date": end_date}), 200)

    return res
