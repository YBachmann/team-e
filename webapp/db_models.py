from flask import current_app
from webapp import db, login_manager, bcrypt
from datetime import date, datetime
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from flask_login import UserMixin
import itsdangerous


@login_manager.user_loader
def load_user(user_id):
    return User.objects(id=int(user_id)).first()


class User(db.Document, UserMixin):
    id = db.IntField(primary_key=True)
    pers_nr = db.IntField(required=True)
    first_name = db.StringField(max_length=50, null=False, required=True)
    last_name = db.StringField(max_length=50, null=False, required=True)
    email = db.EmailField(max_length=100, unique=True,
                          null=False, required=True)
    image_file = db.StringField(max_length=20, null=False, required=True,
                                default="default.jpg")
    password = db.StringField(max_length=60, null=False, required=True)
    street = db.StringField(max_length=50, null=False, required=True)
    house_number = db.IntField(null=False, required=True)
    zip_code = db.IntField(null=False, required=True)
    city = db.StringField(max_length=50, null=False, required=True)
    birthdate = db.DateField(null=False, required=True,
                             default=date.today())
    gender = db.StringField(max_length=2, null=False, required=True)
    department = db.StringField(max_length=50, null=False, required=True)
    position = db.StringField(max_length=50, null=False, required=True)
    salary = db.DecimalField(null=False, required=True)
    time_model = db.DecimalField(null=False, required=True)
    presence = db.IntField(null=False, required=True, default=5)
    state = db.StringField(null=False, required=True, default="aktiv")
    role = db.StringField(null=False, required=True, default="0")
    inactive_reason = db.StringField(max_length=50, default="")
    holidays = db.IntField(null=False, required=True)
    remaining_holidays = db.IntField(null=False, required=True)

    def get_reset_token(self, expires_sec=1800):
        """
        Create a secure token containing the user-id as a payload by using the app secret key

        Args:
            self (User): The user for which a token should be created
            expires_sec=1800 (undefined): Time in seconds after which the token expires
        """

        # Create an itsdangerous serializer with the app secret key
        # It works like the regular JSONWebSignatureSerializer but also records the time of the signing and can be used to expire signatures.
        s = Serializer(current_app.config["SECRET_KEY"], expires_sec)
        
        # Create the token containing the user_id 
        return s.dumps({"user_id": self.id}).decode("utf-8")

    @staticmethod
    def verify_reset_token(token):
        """
        Try to verify the token and return the user that belongs to it

        Args:
            token (undefined): Reset Token to verify

        Return: 
            user (User): Return the db User object if successful. Return None otherwise. 
        """
        # Create an itsdangerous serializer with the app secret key
        s = Serializer(current_app.config["SECRET_KEY"])
        try:
            # Try to load the payload(user_id) from the token
            user_id = s.loads(token)["user_id"]
        except itsdangerous.exc.SignatureExpired:
            return None
        return User.objects(id=user_id).first()


class Timestamp(db.Document):
    #id = db.IntField(primary_key = True)
    user_id = db.IntField(required=True)
    date = db.DateTimeField(required=True)
    kind = db.StringField(max_length=20, required=True)
    pair_id = db.IntField()
