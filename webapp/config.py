import os


class Config:
    SECRET_KEY = os.environ.get(
        "UTIME_SECRET_KEY", "eb475c93503a0fb79e262003916576d0")  # default nur temporär für Abgabe eingefügt

    MONGODB_SETTINGS = {
        "db": "database",
        "host": "mongodb+srv://"
                # default nur temporär für Abgabe eingefügt
                f"{os.environ.get('UTIME_DB_USER', 'uTime_dbUser')}:{os.environ.get('UTIME_DB_PASS', 'uTime_asdf')}"
                # default nur temporär für Abgabe eingefügt
                f"@utimecluter-mlve5.mongodb.net/{os.environ.get('UTIME_DB_NAME_PRODUCTION', 'database')}"
                "?retryWrites=true&w=majority",
        "port": os.environ.get("UTIME_DB_PORT", 27017)
    }

    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 587  # use 465 for SSL
    MAIL_USE_TLS = True
    MAIL_USE_SSL = False
    MAIL_USERNAME = os.environ.get(
        'UTIME_EMAIL_USER', 'noreply.uTime@gmail.com')  # default nur temporär für Abgabe eingefügt
    # default nur temporär für Abgabe eingefügt
    MAIL_PASSWORD = os.environ.get("UTIME_EMAIL_PASS", 'R8nI34vvAaIn9M')


class TestingConfig(Config):

    # Use test_database
    MONGODB_SETTINGS = {
        "db": "database",
        "host": "mongodb+srv://"
                # default nur temporär für Abgabe eingefügt
                f"{os.environ.get('UTIME_DB_USER')}:{os.environ.get('UTIME_DB_PASS', 'uTime_asdf')}"
                # default nur temporär für Abgabe eingefügt
                f"@utimecluter-mlve5.mongodb.net/{os.environ.get('UTIME_DB_NAME_TEST', 'test_database')}"
                "?retryWrites=true&w=majority",
        "port": os.environ.get("UTIME_DB_PORT", 27017)
    }

    DEBUG = False
    TESTING = True
    DEBUG_TB_INTERCEPT_REDIRECTS = False
    WTF_CSRF_ENABLED = False
