from flask import Flask
from flask_mongoengine import MongoEngine
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
from flask_mail import Mail
from webapp.config import Config


db = MongoEngine()

bcrypt = Bcrypt()

login_manager = LoginManager()
login_manager.login_view = "authentication.login"
login_manager.login_message_category = "info"

mail = Mail()


def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)

    db.init_app(app)
    bcrypt.init_app(app)
    login_manager.init_app(app)
    mail.init_app(app)

    from webapp.absence_management.routes import absence_management  # nopep8
    from webapp.authentication.routes import authentication  # nopep8
    from webapp.calendar.routes import calendar  # nopep8
    from webapp.employee_administration.routes import employee_administration  # nopep8
    from webapp.timestamp_overview.routes import timestamp_overview # nopep8
    from webapp.timestamp_dashboard.routes import timestamp_dashboard #nopep8
    from webapp.employee_timestamps.routes import employee_timestamps #nopep8
    from webapp.overview_hours.routes import overview_hours #nopep8
    from webapp.errors.handlers import errors  # nopep8    
    from webapp.main.routes import main  # nopep8

    app.register_blueprint(absence_management)
    app.register_blueprint(authentication)
    app.register_blueprint(calendar)
    app.register_blueprint(employee_administration)
    app.register_blueprint(timestamp_overview)
    app.register_blueprint(timestamp_dashboard)
    app.register_blueprint(employee_timestamps)
    app.register_blueprint(overview_hours)
    app.register_blueprint(errors)
    app.register_blueprint(main)

    return app
