from flask import Blueprint, redirect, request, flash, url_for
from flask_login import login_required, current_user
from flask.templating import render_template
from webapp.db_models import Timestamp
from webapp.db_models import User
import datetime
import json

calendar = Blueprint("calendar", __name__, template_folder="templates")


@calendar.route('/overview_workdays', methods=['GET', 'POST'])
@login_required
def overview_workdays():
    """
        The route that leads to the overview over the workdays of an employee
        Checks whether Personalsachbearbeiter wants to access the website or normal employee
        returns the user-data and the timestamp-data from the database and
        returns the years that are supposed to be shown in the yearpicker and
        returns the bool that decides whether route was accessed by dashboards or by employee_table
    """

    # get pers_nr from URL
    persNr = request.args.get("pers_nr")
    # test if the parameter is empty or not
    if(persNr == None):
        flash("Fehlerhafte Übergabeparameter! Versuchen Sie die Seite erneut zu laden.", "danger")
        return redirect(url_for("timestamp_dashboard.dashboard"))
    else:
        # test if the parameter persnr really is a number or something else
        try:
            int(persNr)
        except ValueError:
            flash(
                "Fehlerhafte Übergabeparameter! Versuchen Sie die Seite erneut zu laden.", "danger")
            return redirect(url_for("timestamp_dashboard.dashboard"))

    # forbid users without access
    if current_user.pers_nr != int(persNr):
        # bool that decides which tab to be marked
        boolPersNr = False
        if current_user.role != "1":
            flash("Zugriff nur für Personalsachbearbeiter", "danger")
            return redirect(url_for("timestamp_dashboard.dashboard"))

    # bool that decides which tab to be marked
    if current_user.pers_nr == int(persNr):
        boolPersNr = True

    # get the past 20 years in an array and return it to use it later in jinja
    now = datetime.datetime.now()
    pastYears = []
    currentYear = now.year
    # if more years neccessary -> increase the range
    for i in range(20):
        pastYears.append(currentYear)
        currentYear -= 1

    # get related timestamp entries from database
    timestamps = Timestamp.objects(user_id=persNr).all().as_pymongo()
    # get name of user with related id
    user = User.objects(pers_nr=persNr).all().as_pymongo()

    # array to save timestamp data
    timestampsArr = []

    # iterate over timestamps and write data in arrays
    i = 0
    for i in range(len(timestamps)):

        timestampDict = {
            "year": timestamps[i]["date"].year,
            "month": timestamps[i]["date"].month,
            "day": timestamps[i]["date"].day,
            "kind": timestamps[i]["kind"]
        }
        timestampsArr.append(timestampDict)

        i += 1

    # check whether there are timestamps or not
    if not timestampsArr:
        flash("Es existieren noch keine Einträge für diesen Benutzer.", "info")


    return render_template('overview_workdays.html', timestampsArr=timestampsArr,
                           user=user, pastYears=pastYears, boolPersNr=boolPersNr)
