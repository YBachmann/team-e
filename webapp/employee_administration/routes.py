from flask import Blueprint, redirect, request
from flask.helpers import flash, url_for
from flask_login import login_required, current_user
from webapp.db_models import User
from flask.templating import render_template
from webapp.employee_administration.forms import EmployeeDetailForm
from webapp.employee_administration.utils import *

# generate the blueprint "employee_administration" with name "employee_administration" and the html template folder "templates"
employee_administration = Blueprint(
    "employee_administration", __name__, template_folder="templates")


@employee_administration.route("/employee_list", methods=["GET", "POST"])
@login_required
def employee_list():
    """
        route that leads to the employee_list (Anwesenheitstableau)
        check whether Personalsachbearbeiter wants to access or normal employee
        return the user-data from the database and the state that is selected
        if Personalsachbearbeiter wants to see all employees in the table, then state = all
        and inactive ones are also selected from the database
        otherwise only the active ones are selected from the db
    """

    # restrict access, employee_list is only accessable for the personal administrator (role: 1)
    if current_user.role == "0":
        flash("Zugriff nur für Personalsachbearbeiter", "danger")
        return redirect(url_for("timestamp_dashboard.dashboard"))
    else:
        # get the value of the state out of the URL
        state = request.args.get("state")
        if state == "all":
            # load all users to the table
            employees = User.objects().all().as_pymongo()
        else:
            # load only active users to the table
            employees = User.objects(state="aktiv").all().as_pymongo()

        return render_template("employee_list.html", employees=employees, state=state)


@employee_administration.route("/employee_detail", methods=["GET", "POST"])
@login_required
def employee_detail():
    # restrict access, employee_detail is only accessable for the personal administrator (role: 1)
    if current_user.role == "0":
        flash("Zugriff nur für Personalsachbearbeiter", "danger")
        return redirect(url_for("timestamp_dashboard.dashboard"))
    else:
        # load everything for the template
        employeeDetailForm = EmployeeDetailForm()

        # get the value of the pers_nr out of the URL, here the personal number of the employee
        pers_nr = request.args.get("pers_nr")
        try:
            if pers_nr:
                # edit existent user
                user = User.objects.get_or_404(pers_nr=pers_nr)
            else:
                # create new user
                user = None
        except ValueError:
            # if pers_nr is not a number
            flash("Personalnummer ungültig", "danger")
            return redirect(url_for("timestamp_dashboard.dashboard"))

        # save the data if valid
        if employeeDetailForm.validate_on_submit():
            if user:
                # update user
                update_user(employeeDetailForm, pers_nr)
                return redirect(url_for("employee_administration.employee_detail", pers_nr=pers_nr))
            else:
                # create new user
                create_new_user(employeeDetailForm)
                return redirect(url_for("employee_administration.employee_list"))
        elif request.method == "GET" and user:
            # load user into form
            load_user(employeeDetailForm, user)
        elif employeeDetailForm.errors:
            # print error in form
            print(employeeDetailForm.errors)

        return render_template("employee_detail.html", form=employeeDetailForm, user=user)


@employee_administration.route("/employee_change_state")
@login_required
def employee_change_state():
    # restrict access, employee_change_state is only accessable for the personal administrator (role: 1)
    if current_user.role == "0":
        flash("Zugriff nur für Personalsachbearbeiter", "danger")
        return redirect(url_for("timestamp_dashboard.dashboard"))
    else:
        # get the value of the pers_nr out of the URL, here the personal number of the employee
        pers_nr = request.args.get("pers_nr")
        if pers_nr is None:
            # if pers_nr is empty or not passed via URL
            flash("Personalnummer fehlt", "danger")
            return redirect(url_for("timestamp_dashboard.dashboard"))

        # try to load user from database
        try:
            user = User.objects.get_or_404(pers_nr=pers_nr)
        except ValueError:
            # if pers_nr is not a number
            flash("Personalnummer ungültig", "danger")
            return redirect(url_for("timestamp_dashboard.dashboard"))

        # get the value of the reason out of the URL, here the reason for the deactivation of an employee
        reason = request.args.get("reason")
        # if user is inactive -> reason is mandatory
        if reason == "null" and user.state == "aktiv":
            flash("Bitte Deaktivierungsgrund angeben", "danger")
            return redirect(url_for("employee_administration.employee_detail", pers_nr=pers_nr))

        if user.state == "aktiv":
            new_state = "inaktiv"
            flash("Benutzerstatus erfolgreich deaktiviert", "success")
        elif user.state == "inaktiv":
            new_state = "aktiv"
            flash("Benutzerstatus erfolgreich aktiviert", "success")

        # update user
        User.objects(pers_nr=pers_nr).update(
            set__inactive_reason=reason,
            set__state=new_state
        )

        return redirect(url_for("employee_administration.employee_detail", pers_nr=pers_nr))
