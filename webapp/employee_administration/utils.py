import secrets
import os
from flask import current_app
from PIL import Image
from flask import redirect
from flask.helpers import flash, url_for
from webapp import bcrypt
from webapp.db_models import User



def save_picture(form_picture):
    """
    Generate a random hex file-name for saving the profile picture with a unique name in the database

    Args:
        form_picture: the form field of the profile picture

    Return:
        return the picture filename as a random hex value + file_extension

    """
    # generate random image file name
    random_hex = secrets.tokenhex(8)
    _, file_extension = os.path.splitext(form_picture.filename)
    picture_filename = random_hex + file_extension
    picture_path = os.path.join(
        current_app.root_path, "static/profile_pics", picture_filename)
    # resize image
    output_size = (125, 125)
    resized_image = Image.open(form_picture)
    resized_image.thumbnail(output_size)
    # save image
    resized_image.save(picture_path)
    return picture_filename


def update_user(employeeDetailForm, pers_nr):
    """
    Update the user in the database with the values of the form fields

    Args:
        employeeDetailForm: the html form which the fields contains
        pers_nr: the personal number of the user to be updated

    """
    if employeeDetailForm.picture.data:
        picture_filename = save_picture(employeeDetailForm.picture.data)
    else:
        picture_filename = "default.jpg"
    # update user
    User.objects(pers_nr=pers_nr).update(
        set__first_name=employeeDetailForm.first_name.data,
        set__last_name=employeeDetailForm.last_name.data,
        set__email=employeeDetailForm.email.data,
        set__image_file=picture_filename,
        set__street=employeeDetailForm.street.data,
        set__house_number=employeeDetailForm.house_number.data,
        set__zip_code=employeeDetailForm.zip_code.data,
        set__city=employeeDetailForm.city.data,
        set__birthdate=employeeDetailForm.birthdate.data,
        set__gender=employeeDetailForm.gender.data,
        set__department=employeeDetailForm.department.data,
        set__position=employeeDetailForm.position.data,
        set__salary=employeeDetailForm.salary.data,
        set__time_model=employeeDetailForm.time_model.data,
        set__role=employeeDetailForm.role.data,
        set__holidays=employeeDetailForm.holidays.data
    )
    flash("Mitarbeiter erfolgreich geändert", "success")


def create_new_user(employeeDetailForm):
    """
    Create a new User. Save all fields of the given form in the database.
    Assign a new pers_nr and id.

    Args:
        employeeDetailForm: the form with containing the fields to be saved in the database

    """
    if employeeDetailForm.picture.data:
        picture_filename = save_picture(employeeDetailForm.picture.data)
    else:
        picture_filename = None
    # create new user, increment id and pers_nr
    hashed_password = bcrypt.generate_password_hash(
        'default').decode("utf-8")
    id = User.objects.count()
    new_user = User(
        id=id,
        pers_nr=id,
        first_name=employeeDetailForm.first_name.data,
        last_name=employeeDetailForm.last_name.data,
        email=employeeDetailForm.email.data,
        image_file=picture_filename,
        password=hashed_password,
        street=employeeDetailForm.street.data,
        house_number=employeeDetailForm.house_number.data,
        zip_code=employeeDetailForm.zip_code.data,
        city=employeeDetailForm.city.data,
        birthdate=employeeDetailForm.birthdate.data,
        gender=employeeDetailForm.gender.data,
        department=employeeDetailForm.department.data,
        position=employeeDetailForm.position.data,
        salary=employeeDetailForm.salary.data,
        time_model=employeeDetailForm.time_model.data,
        role=employeeDetailForm.role.data,
        holidays=employeeDetailForm.holidays.data,
        remaining_holidays=employeeDetailForm.holidays.data
    )
    new_user.save()
    flash("Mitarbeiter erfolgreich hinzugefügt", "success")


def load_user(employeeDetailForm, user):
    """
    Load all data from the database to the form.

    Args:
        employeeDetailForm: the form, which fields have to be filled in
        user: the user, who has to be loaded from the database

    """
    # load user in form
    employeeDetailForm.pers_nr.data = user.pers_nr
    employeeDetailForm.first_name.data = user.first_name
    employeeDetailForm.last_name.data = user.last_name
    employeeDetailForm.email.data = user.email
    employeeDetailForm.street.data = user.street
    employeeDetailForm.house_number.data = user.house_number
    employeeDetailForm.zip_code.data = user.zip_code
    employeeDetailForm.city.data = user.city
    employeeDetailForm.birthdate.data = user.birthdate
    employeeDetailForm.gender.data = user.gender
    employeeDetailForm.department.data = user.department
    employeeDetailForm.position.data = user.position
    employeeDetailForm.salary.data = user.salary
    employeeDetailForm.time_model.data = user.time_model
    employeeDetailForm.state.data = user.state
    employeeDetailForm.role.data = user.role
    employeeDetailForm.inactive_reason.data = user.inactive_reason
    employeeDetailForm.holidays.data = user.holidays
