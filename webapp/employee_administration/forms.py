from flask_wtf import FlaskForm
from wtforms.fields.core import DateField, DecimalField, IntegerField, SelectField, StringField
from flask_wtf.file import FileAllowed, FileField
from wtforms.validators import DataRequired, Email, ValidationError
from wtforms.fields.simple import SubmitField
from webapp.db_models import User


class EmployeeDetailForm(FlaskForm):
    pers_nr = IntegerField('PersNr')
    picture = FileField('Update Profile Picture', validators=[
        FileAllowed(['jpg', 'png'])])
    first_name = StringField('Vorname', description='Vorname',
                             validators=[DataRequired()])
    last_name = StringField('Name', description='Name',
                            validators=[DataRequired()])
    email = StringField('Email', description='Email-Adresse',
                        validators=[DataRequired(), Email()])
    street = StringField('Straße', description='Straße',
                         validators=[DataRequired()])
    house_number = StringField('HausNr', description='HausNr',
                               validators=[DataRequired()])
    zip_code = StringField('PLZ', description='PLZ',
                           validators=[DataRequired()])
    city = StringField('Ort', description='Ort', validators=[DataRequired()])
    birthdate = DateField('Geburtsdatum', id='datepick', description='Geburtsdatum',
                          format='%d.%m.%Y', validators=[DataRequired()])
    gender = SelectField('Geschlecht', description='Geschlecht', choices=[('0', 'Bitte Geschlecht wählen'), (
        '1', 'Männlich'), ('2', 'Weiblich'), ('3', 'Divers')], default=None, validators=[DataRequired()])
    department = StringField(
        'Abteilung', description='Abteilung', validators=[DataRequired()])
    position = StringField(
        'Position', description='Position', validators=[DataRequired()])
    salary = DecimalField('Gehalt', description='Gehalt',
                          validators=[DataRequired()])
    time_model = DecimalField(
        'Zeitmodell', description='Zeitmodell', validators=[DataRequired()])
    presence = IntegerField('Anwesenheitsstatus',
                            description='Anwesenheitsstatus')
    state = StringField('Status', description='Status')
    role = SelectField('Benutzerrolle', description='Benutzerrolle', choices=[('0', 'Mitarbeiter'), (
        '1', 'Personalsachbearbeiter')], default=None, validators=[DataRequired()])
    inactive_reason = StringField('Grund', description='Grund')
    holidays = IntegerField('Urlaubstage',
                            description='Urlaubstage')

    save = SubmitField('Speichern')

    def validate_email(self, email_form):
        # check if a user with this email already exists in the db
        user = User.objects(email=email_form.data).first()
        if user is not None:
            if self.pers_nr.data != user.pers_nr:
                raise ValidationError(
                    "Email already is taken. Please choose a different one. ")
