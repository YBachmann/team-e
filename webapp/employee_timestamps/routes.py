from flask import (Blueprint, redirect, request, url_for)
from flask.helpers import flash
from flask.templating import render_template
from flask_login import login_user, logout_user, current_user, login_required
from flask import redirect,  request, jsonify, make_response, json
from webapp.db_models import Timestamp
from datetime import datetime, timedelta
from mongoengine import Q
from webapp.main.utils import *


employee_timestamps = Blueprint(
    'employee_timestamps', __name__, template_folder="templates")


@employee_timestamps.route("/")
@employee_timestamps.route("/employee_timestamps", methods=["POST", "GET"])
@login_required
def employee_overview():
    # restrict page to be only used by "personalsachbearbeiter"-role (1)
    if current_user.role == "0":
        flash("Zugriff nur für Personalsachbearbeiter", "danger")
        return redirect(url_for("timestamp_dashboard.dashboard"))

    elif current_user.role == "1":
        # get persnr
        pers_nr = request.args.get("pers_nr")

        # get Timestamps off current month til today
        datum = datetime.now()
        monat = datum.month
        current_day = datum.day
        # create start day on first day of month and end day at current day of month
        start = datetime(datum.year, monat, 1)
        end = datetime(datum.year, monat, current_day)

        all_days = []

        # Iterate over every day from start date to end date.
        # single_date is a datetime object
        for single_date in daterange(start, end):
            # query all timestamps from one day by searching for every entry between morning and evening
            morning = single_date.replace(hour=0, minute=0, second=0)
            evening = single_date.replace(hour=23, minute=59, second=59)
            # query for timestamps where the date is 'greater than or equal'(gte) morning date and 'less than or equal' (lte) evening date
            day = Timestamp.objects(Q(user_id=pers_nr) & Q(date__gte=morning) & Q(
                date__lte=evening)).order_by('date').as_pymongo()
            # add the day to an array containing all days of the month
            all_days.append(day)

        return render_template('employee_overview.html', timestamps=json.dumps(all_days), startdate=start)


@employee_timestamps.route("/employee_timestamps/get-timestamps", methods=["POST", "GET"])
@login_required
def get_timestamps():
    # restrict page to be only used by "personalsachbearbeiter"-role (1)
    if current_user.role == "0":
        flash("Zugriff nur für Personalsachbearbeiter", "danger")
        return redirect(url_for("timestamp_dashboard.dashboard"))

    elif current_user.role == "1":
        req = request.get_json()
        pers_nr = req["pers_nr"]

        # convert time string to datetime object
        start = datetime.strptime(req["from"], '%d.%m.%Y')
        end_date = datetime.strptime(req["to"], '%d.%m.%Y')
        end = datetime(end_date.year, end_date.month, end_date.day)

        all_days = []

        # Iterate over every day from start date to end date.
        # single_date is a datetime object
        for single_date in daterange(start, end):
            # query all timestamps from one day by searching for every entry between morning and evening
            morning = single_date.replace(hour=0, minute=0, second=0)
            evening = single_date.replace(hour=23, minute=59, second=59)
            # query for timestamps where the date is 'greater than or equal'(gte) morning date and 'less than or equal' (lte) evening date
            day = Timestamp.objects(Q(user_id=pers_nr) & Q(date__gte=morning) & Q(
                date__lte=evening)).order_by('date').as_pymongo()
            # add the day to an array containing all days of the timespan
            all_days.append(day)
        # create a response containing all the timestamps and the startdate
        res = make_response(jsonify({"timestamps": json.dumps(
            all_days), "startdate": start}), 200)

        return res


@employee_timestamps.route("/employee_timestamps/edit-timestamps", methods=["POST", "GET"])
@login_required
def edit_timestamps():
    # restrict page to be only used by "personalsachbearbeiter"-role (1)
    if current_user.role == "0":
        flash("Zugriff nur für Personalsachbearbeiter", "danger")
        return redirect(url_for("timestamp_dashboard.dashboard"))
    elif current_user.role == "1":

        # get parameters
        pers_nr = request.args.get("pers_nr")
        date = request.args.get("date")
        # check for faulty parameters
        if pers_nr is None or date is None:
            flash("Fehlerhafte Parameter", "warning")
            return redirect(url_for('employee_timestamps.employee_overview'))

        # convert the requested day-string to a datetime object
        dayDate = datetime.strptime(date, '%d.%m.%Y')
        # query all timestamps from one day by searching for every entry between morning and evening
        morning = dayDate.replace(hour=0, minute=0, second=0)
        evening = dayDate.replace(hour=23, minute=59, second=59)
        # query for timestamps where the date is 'greater than or equal'(gte) morning date and 'less than or equal' (lte) evening date
        timestamps = Timestamp.objects(Q(user_id=pers_nr) & Q(
            date__gte=morning) & Q(date__lte=evening)).order_by('date').as_pymongo()

        return render_template("edit_timestamps.html", timestamps=timestamps, pers_nr=pers_nr, date=date)


@employee_timestamps.route("/employee_timestamps/delete-timestamps", methods=["POST", "GET"])
@login_required
def delete_timestamps():
    # restrict page to be only used by "personalsachbearbeiter"-role (1)
    if current_user.role == "0":
        flash("Zugriff nur für Personalsachbearbeiter", "danger")
        return redirect(url_for("timestamp_dashboard.dashboard"))

    elif current_user.role == "1":

        # get parameters
        pers_nr = request.args.get("pers_nr")
        pair_id = request.args.get("pair_id")
        # check for faulty parameters
        if pair_id is "":
            flash("Fehlerhafte Zeitstempel. Keine Paar-ID", "danger")
            return redirect(request.referrer, 307)
        # try to query the pair
        try:
            pair = Timestamp.objects(Q(user_id=pers_nr) & Q(pair_id=pair_id))
        except Exception as e:
            flash("Fehler beim Löschen", "danger")
            return redirect(request.referrer)
        # delete the pair
        pair.delete()

        flash("Zeitabschnitt erfolgreich gelöscht", "success")
        # Redirect to last page (including parameters)
        return redirect(request.referrer, 200)


@employee_timestamps.route("/employee_timestamps/change-timestamps", methods=["POST", "GET"])
@login_required
def change_timestamps():
    # restrict page to be only used by "personalsachbearbeiter"-role (1)
    if current_user.role == "0":
        flash("Zugriff nur für Personalsachbearbeiter", "danger")
        return redirect(url_for("timestamp_dashboard.dashboard"))

    elif current_user.role == "1":
        # get Parameters
        pers_nr = request.args.get("pers_nr")
        stamp_from = request.args.get("stamp_from")
        stamp_to = request.args.get("to")
        date = request.args.get("date")
        pair_id = request.args.get("pair_id")

        # check for empty parameters
        if pers_nr is None or stamp_from is None or stamp_to is None or date is None or pair_id is None:
            flash("Fehlerhafte Parameter", "danger")
            return redirect(request.referrer, 307)
        # check for empty pair id
        if pair_id is "":
            flash("Fehlerhafte Zeitstempel. Keine Paar-ID", "danger")
            return redirect(request.referrer, 307)

        # convert strings to datetime objects
        date_from = datetime.strptime(date + stamp_from, '%d.%m.%Y%H:%M')
        date_to = datetime.strptime(date + stamp_to, '%d.%m.%Y%H:%M')

        # check if date_from is lager than date to (stamping in after stamping out)
        if(date_from > date_to):
            flash('Zeitstempel "von" ist nach Zeitstempel "bis"', "danger")
            return redirect(request.referrer, 307)
        else:

            # check if changed times overlap with timestamps in the database
            # get every timestamp in the changed timespan
            overlap = Timestamp.objects(Q(user_id=pers_nr) & Q(date__gte=date_from) & Q(
                date__lte=date_to) & Q(pair_id__ne=pair_id)).as_pymongo()
            # Alert user if timestamps overlap
            if overlap:
                flash(
                    "Zeitabschnitte überschneiden sich mit anderen Zeitstempeln", "danger")
                return redirect(request.referrer, 307)
            else:
                # change entrys
                # get entrys to be changed
                edit_from = Timestamp.objects(Q(user_id=pers_nr) & Q(
                    pair_id=pair_id) & Q(kind="einstempeln"))
                print(edit_from.as_pymongo())
                edit_to = Timestamp.objects(Q(user_id=pers_nr) & Q(
                    pair_id=pair_id) & Q(kind="ausstempeln"))
                print(edit_to.as_pymongo())

                # update entrys
                edit_from.update(date=date_from)
                edit_to.update(date=date_to)
                flash("Zeitstempel erfolgreich geändert", "success")

        # back to the last page
        return redirect(request.referrer, 200)


@employee_timestamps.route("/employee_timestamps/add-timestamps", methods=["POST", "GET"])
@login_required
def add_timestamps():
    # restrict page to be only used by "personalsachbearbeiter"-role (1)
    if current_user.role == "0":
        flash("Zugriff nur für Personalsachbearbeiter", "danger")
        return redirect(url_for("timestamp_dashboard.dashboard"))

    elif current_user.role == "1":
        # get parameters
        pers_nr = request.args.get("pers_nr")
        stamp_from = request.args.get("stamp_from")
        stamp_to = request.args.get("to")
        date = request.args.get("date")
        if pers_nr is None or stamp_from is None or stamp_to is None or date is None:
            flash("Fehlerhafte Parameter", "danger")
            return redirect(request.referrer)

        # convert strings to datetime objects
        date_from = datetime.strptime(date + stamp_from, '%d.%m.%Y%H:%M')
        print(date_from)
        date_to = datetime.strptime(date + stamp_to, '%d.%m.%Y%H:%M')
        print(date_to)
        if(date_from > date_to):
            flash('Zeitstempel "von" ist nach Zeitstempel "bis"', "danger")
            return redirect(request.referrer, 307)
        else:
            # check if added timestamp-Pair overlaps with timestamps in the database
            overlap = Timestamp.objects(Q(user_id=pers_nr) & Q(
                date__gte=date_from) & Q(date__lte=date_to)).as_pymongo()
            print(overlap)
            if overlap:
                flash(
                    "Zeitabschnitte überschneiden sich mit anderen Zeitstempeln", "danger")
                return redirect(request.referrer, 307)
            else:
                # create new entry
                # set pair_id
                pair_id = Timestamp.objects(Q(user_id=pers_nr) & Q(
                    kind="einstempeln")).order_by("-pair_id").as_pymongo()

                if pair_id:
                    pair_id = (pair_id[0]["pair_id"])+1
                else:
                    pair_id = 1

                # add a new timestamps
                stamp_from = Timestamp(
                    date=date_from, kind="einstempeln", user_id=pers_nr, pair_id=pair_id)
                stamp_to = Timestamp(
                    date=date_to, kind="ausstempeln", user_id=pers_nr, pair_id=pair_id)
                stamp_from.save()
                stamp_to.save()
                flash("Zeitstempel erfolgreich hinzugefügt", "success")

        # Back to last page (including the parameters)
        return redirect(request.referrer, 200)
