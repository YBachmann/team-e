
from flask import Blueprint
from flask.templating import render_template
from flask_login import login_required


main = Blueprint("main", __name__, template_folder="templates")


@main.route("/account")
@login_required
def account():
    return render_template("account.html")
