from datetime import timedelta


def daterange(start_date, end_date):
    """
    Generator for yielding single dates within a date range

    Args:
        start_date (datetime.datetime): Start of the date range (including start_date)
        end_date (datetime.datetime): End of the date range (including start_date)

    Yield: 
        day (datetime.datetime): single day within the specified date range
    """

    for n in range(int((end_date - start_date).days) + 1):
        yield start_date + timedelta(n)
