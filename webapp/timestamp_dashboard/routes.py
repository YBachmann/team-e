from flask import (Blueprint, redirect, request, url_for)
from flask.helpers import flash
from flask.templating import render_template
from flask_login import login_user, logout_user, current_user, login_required
from flask import redirect,  request, jsonify, make_response, json
from webapp.db_models import User
from webapp.db_models import Timestamp
from datetime import datetime, timedelta
from mongoengine import Q
from webapp.main.utils import *
import json
from bson.json_util import dumps

timestamp_dashboard = Blueprint(
    'timestamp_dashboard', __name__, template_folder="templates")


@timestamp_dashboard.route("/")
@timestamp_dashboard.route("/dashboard")
@login_required
def dashboard():
    # get pers_nr of current user
    persNr = current_user.pers_nr
    # get related timestamp entries from database
    timestamps = Timestamp.objects(user_id=persNr).all().as_pymongo()
    # get name of user with related id
    user = User.objects(pers_nr=persNr).all().as_pymongo()
    # array to save timestamp data
    year = []
    month = []
    day = []
    kind = []
    timestampsArr = []

    # iterate over timestamps and write data in arrays
    i = 0
    for i in range(len(timestamps)):
        timestampDict = {
            "year": timestamps[i]["date"].year,
            "month": timestamps[i]["date"].month,
            "day": timestamps[i]["date"].day,
            "kind": timestamps[i]["kind"]
        }
        timestampsArr.append(timestampDict)
        i += 1

    # Get Timestamps of the last 7 hours (legal work time is 6 hours without a break in germany, the timestampdisplay on the dashboard displays 8 hours)
    today = datetime.now()
    legal_worktime = timedelta(hours=7)
    begin = today - legal_worktime
    # User ID = 4 für Tests
    day = Timestamp.objects(Q(user_id=persNr) & Q(date__gte=begin) & Q(
        date__lte=today)).order_by('date').as_pymongo()
    # get presence
    presence = user[0]["presence"]

    if(presence == 0):
        lastKind = "einstempeln"
    else:
        lastKind = "ausstempeln"

    return render_template('dashboard.html', timestampsArr=timestampsArr, user=user, lastKind=lastKind, startdate=today, stampInfo=dumps(day))


@timestamp_dashboard.route("/dashboard/add-timestamps", methods=["POST", "GET"])
def add_timestamps():

    pers_nr = current_user.pers_nr
    kind = request.args.get("kind")
    date = datetime.now()

    # check for faulty parameters
    if(pers_nr is None or kind is None or date is None):
        flash("Fehlerhafte Parameter", "danger")

    pair_id = Timestamp.objects(Q(user_id=pers_nr) & Q(
        kind="ausstempeln")).order_by("-date").as_pymongo()

    # if there is no pair_id, set it to 1
    try:
        pair_id = (pair_id[0]["pair_id"])+1
    except KeyError:
        pair_id = 1

    # get presence
    user = User.objects(pers_nr=pers_nr).all()
    presence = user[0]["presence"]
    # change employee status to "anwesend" (0) or "abwesend" (5)
    if (kind == "einstempeln" and presence == 5):
        user.update(presence=0)

    elif (kind == "ausstempeln" and presence == 0):
        user.update(presence=5)

    # enter new timestamp
    stamp = Timestamp(date=date, kind=kind, user_id=pers_nr, pair_id=pair_id)
    stamp.save()
    flash("Zeitstempel erfolgreich eingetragen", "success")

    return redirect(request.referrer)
